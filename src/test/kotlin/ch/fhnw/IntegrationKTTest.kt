package ch.fhnw

import ch.fhnw.checker.UndeclaredIdentifierError
import ch.fhnw.symboltable.DuplicateDeclarationError
import ch.fhnw.vm.Code
import ch.fhnw.vm.CodeArray
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class IntegrationKTTest {

    @AfterEach
    fun reset() {
        Code.codeArray = CodeArray()
    }

    @Test
    fun `compile Extension`() {
        main(arrayOf("src/main/resources/Extension.iml"))
    }

    @Test
    fun `compile Assoc`() {
        main(arrayOf("src/main/resources/Assoc.iml"))
    }

    @Test
    fun `compile Euclid`() {
        main(arrayOf("src/main/resources/Euclid.iml"))
    }

    @Test
    fun `compile EuclidExtNat`() {
        main(arrayOf("src/main/resources/EuclidExtNat.iml"))
    }

    @Test
    fun `compile Factorial`() {
        main(arrayOf("src/main/resources/Factorial.iml"))
    }

    @Test
    fun `compile GcdDef`() {
        main(arrayOf("src/main/resources/GcdDef.iml"))
    }

    @Test
    fun `compile intDiv`() {
        main(arrayOf("src/main/resources/intDiv.iml"))
    }

    @Test
    fun `compile Add17`() {
        main(arrayOf("src/main/resources/Add17.iml"))
    }

    @Test
    fun `compile Add17Fun`() {
        main(arrayOf("src/main/resources/Add17Fun.iml"))
    }

    @Test
    fun `compile Add17Proc`() {
        main(arrayOf("src/main/resources/Add17Proc.iml"))
    }

    @Test
    fun `compile Add17Proc2`() {
        main(arrayOf("src/main/resources/Add17Proc2.iml"))
    }

    @Test
    fun `compile Add17Proc3`() {
        main(arrayOf("src/main/resources/Add17Proc3.iml"))
    }

    @Test
    fun `compile Add17Proc4`() {
        main(arrayOf("src/main/resources/Add17Proc4.iml"))
    }

    @Test
    fun `compile Expr01`() {
        main(arrayOf("src/main/resources/Expr01.iml"))
    }

    @Test
    fun `compile IfCmd`() {
        main(arrayOf("src/main/resources/IfCmd.iml"))
    }

    @Test
    fun `compile WhileCmd`() {
        main(arrayOf("src/main/resources/WhileCmd.iml"))
    }

    @Test
    fun `compile LetInit`(){
        main(arrayOf("src/main/resources/LetInit.iml"))
    }

    @Test
    fun `throw on duplicate Declaration`(){
        Assertions.assertThrows(DuplicateDeclarationError::class.java) { main(arrayOf("src/main/resources/DuplicateDeclaration.iml")) }
    }

    @Test
    fun `throw on undeclared Declaration`(){
        Assertions.assertThrows(UndeclaredIdentifierError::class.java) { main(arrayOf("src/main/resources/UndeclaredIdentifier.iml")) }
    }
}
