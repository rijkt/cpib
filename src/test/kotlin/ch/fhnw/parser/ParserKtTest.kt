package ch.fhnw.parser

import ch.fhnw.tokens.*
import ch.fhnw.tokens.groups.*
import org.junit.jupiter.api.Test

internal class ParserKtTest {

    @Test
    fun `parse example program`() {
        val program = listOf(
            ProgramToken(),
            IdentifierToken("intDiv"),
            LParenToken(),
            FlowToken(FlowMode.IN),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("m"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.IN),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("n"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.OUT),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("q"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.OUT),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("r"),
            ColonToken(),
            TypeToken(Type.INT64),
            RParenToken(), // end of program parameter list
            GlobalToken(),
            ProcToken(),
            IdentifierToken("divide"),
            LParenToken(),
            FlowToken(FlowMode.IN),
            MechModeToken(MechMode.COPY),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("m"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.IN),
            MechModeToken(MechMode.COPY),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("n"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.OUT),
            MechModeToken(MechMode.REF),
            ChangeModeToken(ChangeMode.VAR),
            IdentifierToken("q"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.OUT),
            MechModeToken(MechMode.REF),
            ChangeModeToken(ChangeMode.VAR),
            IdentifierToken("r"),
            ColonToken(),
            TypeToken(Type.INT64),
            RParenToken(), // end of procedure parameter list
            DoToken(),
            IdentifierToken("q"),
            InitToken(),
            BecomesToken(),
            LiteralToken(0),
            SemicolonToken(),
            IdentifierToken("r"),
            InitToken(),
            BecomesToken(),
            IdentifierToken("m"),
            SemicolonToken(),
            WhileToken(),
            IdentifierToken("r"),
            RelOprToken(Operator.GE),
            IdentifierToken("n"),
            DoToken(),
            IdentifierToken("q"),
            BecomesToken(),
            IdentifierToken("q"),
            AddOprToken(Operator.PLUS),
            LiteralToken(1),
            SemicolonToken(),
            IdentifierToken("r"),
            BecomesToken(),
            IdentifierToken("r"),
            AddOprToken(Operator.MINUS),
            IdentifierToken("n"),
            EndWhileToken(),
            EndProcToken(),
            DoToken(),
            CallToken(),
            IdentifierToken("divide"),
            LParenToken(),
            IdentifierToken("m"),
            CommaToken(),
            IdentifierToken("n"),
            CommaToken(),
            IdentifierToken("q"),
            InitToken(),
            CommaToken(),
            IdentifierToken("r"),
            InitToken(),
            RParenToken(),
            EndProgramToken(),
            SentinelToken()
        )

        val parser = Parser(program)

        parser.parse()
    }
}