package ch.fhnw.scanner

import ch.fhnw.tokens.*
import ch.fhnw.tokens.groups.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class ScannerKtTest {

    @Test
    fun `input should not be empty`() {
        Assertions.assertThrows(AssertionError::class.java) {
            scan("")
        }
    }

    @Test
    fun `input should end with newline`() {
        Assertions.assertThrows(AssertionError::class.java) {
            scan("no newline")
        }
    }

    @Test
    fun `input ending with newline is valid`() {
        scan("newline\n")
    }

    @Test
    fun `read identifier`() {
        val identifier = "asdf"

        val actual = scan("${identifier}\n")

        val expected = listOf(IdentifierToken(identifier), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read identifier with numbers`() {
        val identifier = "asdf1234"

        val actual = scan("${identifier}\n")

        val expected = listOf(IdentifierToken(identifier), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read identifier with case mixing`() {
        val identifier = "asDF1234"

        val actual = scan("${identifier}\n")

        val expected = listOf(IdentifierToken(identifier), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read multiple identifiers`() {
        val identifier1 = "asDF1234"
        val identifier2 = "asDF1234"

        val actual = scan("$identifier1 ${identifier2}\n")

        val expected = listOf(IdentifierToken(identifier1), IdentifierToken(identifier2), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read apostrophe identifier at the end`() {
        val identifier1 = "a'"

        val actual = scan("$identifier1\n")

        val expected = listOf(IdentifierToken(identifier1), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read underline identifier at the end`() {
        val identifier1 = "a_"

        val actual = scan("$identifier1\n")

        val expected = listOf(IdentifierToken(identifier1), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read apostrophe identifier in the middle`() {
        val identifier1 = "a'a"

        val actual = scan("$identifier1\n")

        val expected = listOf(IdentifierToken(identifier1), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read underline identifier in the middle`() {
        val identifier1 = "a_a"

        val actual = scan("$identifier1\n")

        val expected = listOf(IdentifierToken(identifier1), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `identifier should not start with apostrophe`() {
        Assertions.assertThrows(AssertionError::class.java) {
            scan("'")
        }
    }

    @Test
    fun `identifier should not start with underline`() {
        Assertions.assertThrows(AssertionError::class.java) {
            scan("_")
        }
    }

    @Test
    fun `read literal`() {
        val literal = 67

        val actual = scan("${literal}\n")

        val expected = listOf(LiteralToken(literal), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read multiple literals`() {
        val literal1 = 67
        val literal2 = 42

        val actual = scan("$literal1 ${literal2}\n")

        val expected = listOf(LiteralToken(literal1), LiteralToken(literal2), SentinelToken())
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read too large a number`() {
        val literal: Long = Int.MAX_VALUE + 1L
        Assertions.assertThrows(LexicalError::class.java) {
            scan("${literal}\n")
        }
    }

    @Test
    fun `read unsupported symbol`() {
        val symbol = "€"
        Assertions.assertThrows(LexicalError::class.java) {
            scan("${symbol}\n")
        }
    }

    @Test
    fun `read complete program`() {
        val program = "while x36  <=67 do\n\tx := x-1\n endwhile\n"

        val actual = scan(program)

        val expected = listOf(
            WhileToken(),
            IdentifierToken("x36"),
            RelOprToken(Operator.LE),
            LiteralToken(67),
            DoToken(),
            IdentifierToken("x"),
            BecomesToken(),
            IdentifierToken("x"),
            AddOprToken(Operator.MINUS),
            LiteralToken(1),
            EndWhileToken(),
            SentinelToken()
        )
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read letter to grandma`() {
        val program = "Liebe Grossmutter!\nZu Deinem 67-ten Geburtstag\n"

        val actual = scan(program)

        val expected = listOf(
            IdentifierToken("Liebe"),
            IdentifierToken("Grossmutter"),
            ExclamationMarkToken(),
            IdentifierToken("Zu"),
            IdentifierToken("Deinem"),
            LiteralToken(67),
            AddOprToken(Operator.MINUS),
            IdentifierToken("ten"),
            IdentifierToken("Geburtstag"),
            SentinelToken()
        )

        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `read example program`() {
        val program = """program intDiv(in const m:int64, in const n:int64,
                                        out const q:int64, out const r:int64)
                         global
                            proc divide(in copy const m:int64, in copy const n:int64,
                                        out ref var q:int64, out ref var r:int64)
                            do
                                q init := 0;
                                r init := m;
                                while r >= n do
                                    q := q + 1;
                                    r := r - n
                                endwhile
                            endproc
                        do
                            call divide(m, n, q init, r init)
                        endprogram
""" // newline needs to be last character before end

        val actual = scan(program)

        val expected = listOf(
            ProgramToken(),
            IdentifierToken("intDiv"),
            LParenToken(),
            FlowToken(FlowMode.IN),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("m"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.IN),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("n"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.OUT),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("q"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.OUT),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("r"),
            ColonToken(),
            TypeToken(Type.INT64),
            RParenToken(), // end of program parameter list
            GlobalToken(),
            ProcToken(),
            IdentifierToken("divide"),
            LParenToken(),
            FlowToken(FlowMode.IN),
            MechModeToken(MechMode.COPY),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("m"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.IN),
            MechModeToken(MechMode.COPY),
            ChangeModeToken(ChangeMode.CONST),
            IdentifierToken("n"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.OUT),
            MechModeToken(MechMode.REF),
            ChangeModeToken(ChangeMode.VAR),
            IdentifierToken("q"),
            ColonToken(),
            TypeToken(Type.INT64),
            CommaToken(),
            FlowToken(FlowMode.OUT),
            MechModeToken(MechMode.REF),
            ChangeModeToken(ChangeMode.VAR),
            IdentifierToken("r"),
            ColonToken(),
            TypeToken(Type.INT64),
            RParenToken(), // end of procedure parameter list
            DoToken(),
            IdentifierToken("q"),
            InitToken(),
            BecomesToken(),
            LiteralToken(0),
            SemicolonToken(),
            IdentifierToken("r"),
            InitToken(),
            BecomesToken(),
            IdentifierToken("m"),
            SemicolonToken(),
            WhileToken(),
            IdentifierToken("r"),
            RelOprToken(Operator.GE),
            IdentifierToken("n"),
            DoToken(),
            IdentifierToken("q"),
            BecomesToken(),
            IdentifierToken("q"),
            AddOprToken(Operator.PLUS),
            LiteralToken(1),
            SemicolonToken(),
            IdentifierToken("r"),
            BecomesToken(),
            IdentifierToken("r"),
            AddOprToken(Operator.MINUS),
            IdentifierToken("n"),
            EndWhileToken(),
            EndProcToken(),
            DoToken(),
            CallToken(),
            IdentifierToken("divide"),
            LParenToken(),
            IdentifierToken("m"),
            CommaToken(),
            IdentifierToken("n"),
            CommaToken(),
            IdentifierToken("q"),
            InitToken(),
            CommaToken(),
            IdentifierToken("r"),
            InitToken(),
            RParenToken(),
            EndProgramToken(),
            SentinelToken()
        )
        Assertions.assertEquals(expected, actual)
    }
}
