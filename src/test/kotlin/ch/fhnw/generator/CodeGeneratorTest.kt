package ch.fhnw.generator

import ch.fhnw.main
import ch.fhnw.vm.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class CodeGeneratorTest {

    @AfterEach
    fun reset() {
        Code.codeArray = CodeArray()
    }

    @Test
    fun `compile Add17`() {
        main(arrayOf("src/main/resources/Add17.iml"))

        val expected = arrayOfNulls<IInstr>(1024)
        expected[0] = AllocBlock(1000)
        expected[1] = LoadImInt(1000)
        expected[2] = InputInt("Int")
        expected[3] = LoadImInt(1000)
        expected[4] = LoadImInt(1000)
        expected[5] = Deref()
        expected[6] = LoadImInt(1000)
        expected[7] = AddInt()
        expected[8] = Store()
        expected[9] = LoadImInt(1000)
        expected[10] = Deref()
        expected[11] = OutputInt("Int")
        expected[12] = Stop()

        for (i in 0..expected.size - 1) {
            if (expected[i] != null) {
                Assertions.assertEquals(expected[i]!!::class, Code.codeArray.get(i)!!::class)
            }
        }
    }

    @Test
    fun `compile Add17Fun`() {
        main(arrayOf("src/main/resources/Add17Fun.iml"))

        val expected = arrayOfNulls<IInstr>(1024)
        expected[0] = AllocBlock(1000)
        expected[1] = LoadImInt(1000)
        expected[2] = InputInt("Int")
        expected[3] = AllocBlock(1000)
        expected[4] = LoadImInt(1000)
        expected[5] = Deref()
        expected[6] = Call(1000, "f")
        expected[7] = OutputInt("Int")
        expected[8] = Stop()
        expected[9] = AllocBlock(1000)
        expected[10] = LoadAddrRel(1000)
        expected[11] = LoadAddrRel(1000)
        expected[12] = Deref()
        expected[13] = LoadImInt(1000)
        expected[14] = AddInt()
        expected[15] = Store()
        expected[16] = Return(1000)

        for (i in 0..expected.size - 1) {
            if (expected[i] != null) {
                // Use to debug:
                // println(expected[i]!!::class.toString() + " vs " + Code.codeArray.get(i + 1)!!::class.toString())
                Assertions.assertEquals(expected[i]!!::class, Code.codeArray.get(i)!!::class)
            }
        }
    }

    @Test
    fun `compile Add17Proc`() {
        main(arrayOf("src/main/resources/Add17Proc.iml"))
    }

    @Test
    fun `compile Add17Proc2`() {
        main(arrayOf("src/main/resources/Add17Proc2.iml"))
    }

    @Test
    fun `compile Add17Proc3`() {
        main(arrayOf("src/main/resources/Add17Proc3.iml"))
    }

    @Test
    fun `compile Add17Proc4`() {
        main(arrayOf("src/main/resources/Add17Proc4.iml"))
    }

    @Test
    fun `compile Expr01`() {
        main(arrayOf("src/main/resources/Expr01.iml"))

        val expected = arrayOfNulls<IInstr>(1024)
        expected[0] = AllocBlock(1000)
        expected[1] = AllocBlock(1000)
        expected[2] = AllocBlock(1000)
        expected[3] = LoadImInt(1000)
        expected[4] = InputInt("Int")
        expected[5] = LoadImInt(1000)
        expected[6] = LoadImInt(1000)
        expected[7] = Deref()
        expected[8] = LoadImInt(1000)
        expected[9] = Deref()
        expected[10] = MultInt()
        expected[11] = LoadImInt(1000)
        expected[12] = LoadImInt(1000)
        expected[13] = Deref()
        expected[14] = MultInt()
        expected[15] = AddInt()
        expected[16] = LoadImInt(1000)
        expected[17] = AddInt()
        expected[18] = Store()
        expected[19] = LoadImInt(1000)
        expected[20] = Deref()
        expected[21] = OutputInt("Int")
        expected[22] = LoadImInt(1000)
        expected[23] = LoadImInt(1000)
        expected[24] = Deref()
        expected[25] = LoadImInt(1000)
        expected[26] = AddInt()
        expected[27] = LoadImInt(1000)
        expected[28] = Deref()
        expected[29] = LoadImInt(1000)
        expected[30] = AddInt()
        expected[31] = MultInt()
        expected[32] = Store()
        expected[33] = LoadImInt(1000)
        expected[34] = Deref()
        expected[35] = OutputInt("Int")
        expected[36] = LoadImInt(1000)
        expected[37] = Deref()
        expected[38] = LoadImInt(1000)
        expected[39] = Deref()
        expected[40] = EqInt()
        expected[41] = OutputBool("Bool")
        expected[42] = Stop()

        for (i in 0..expected.size - 1) {
            if (expected[i] != null) {
                Assertions.assertEquals(expected[i]!!::class, Code.codeArray.get(i)!!::class)
            }
        }
    }

    @Test
    fun `compile IfCmd`() {
        main(arrayOf("src/main/resources/IfCmd.iml"))

        val expected = arrayOfNulls<IInstr>(1024)
        expected[0] = AllocBlock(1000)
        expected[1] = AllocBlock(1000)
        expected[2] = LoadImInt(1000)
        expected[3] = InputInt("Int")
        expected[4] = LoadImInt(1000)
        expected[5] = Deref()
        expected[6] = LoadImInt(1000)
        expected[7] = GtInt()
        expected[8] = CondJump(1000)
        expected[9] = LoadImInt(1000)
        expected[10] = LoadImInt(1000)
        expected[11] = Store()
        expected[12] = UncondJump(1000)
        expected[13] = LoadImInt(1000)
        expected[14] = LoadImInt(1000)
        expected[15] = Store()
        expected[16] = LoadImInt(1000)
        expected[17] = Deref()
        expected[18] = OutputInt("Int")
        expected[19] = Stop()

        for (i in 0..expected.size - 1) {
            if (expected[i] != null) {
                Assertions.assertEquals(expected[i]!!::class, Code.codeArray.get(i)!!::class)
            }
        }
    }

    @Test
    fun `compile WhileCmd`() {
        main(arrayOf("src/main/resources/WhileCmd.iml"))

        val expected = arrayOfNulls<IInstr>(1024)
        expected[0] = AllocBlock(1000)
        expected[1] = LoadImInt(1000)
        expected[2] = InputInt("Int")
        expected[3] = LoadImInt(1000)
        expected[4] = Deref()
        expected[5] = LoadImInt(1000)
        expected[6] = GtInt()
        expected[7] = CondJump(1000)
        expected[8] = LoadImInt(1000)
        expected[9] = LoadImInt(1000)
        expected[10] = Deref()
        expected[11] = LoadImInt(1000)
        expected[12] = SubInt()
        expected[13] = Store()
        expected[14] = UncondJump(1000)
        expected[15] = LoadImInt(1000)
        expected[16] = Deref()
        expected[17] = OutputInt("Int")
        expected[18] = Stop()

        for (i in 0..expected.size - 1) {
            if (expected[i] != null) {
                Assertions.assertEquals(expected[i]!!::class, Code.codeArray.get(i)!!::class)
            }
        }
    }
}
