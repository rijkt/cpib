package ch.fhnw.checker

class UndeclaredIdentifierError(message: String): Error(message)