package ch.fhnw.checker

data class TypeError(val msg: String?) : Error(msg)
