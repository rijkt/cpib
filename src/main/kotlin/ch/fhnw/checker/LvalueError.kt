package ch.fhnw.checker

data class LvalueError(val msg: String): Error(msg)
