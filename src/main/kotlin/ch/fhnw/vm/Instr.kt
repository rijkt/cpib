package ch.fhnw.vm

interface IInstr {
    fun toExecInstr(vm: VirtualMachine): IExecInstr
}

/**
 * Executable vm instructions with access to stack and storage
 */
interface IExecInstr : IInstr {
    fun execute()
}

open class AddInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.AddIntExec()
    override fun toString(): String {
        return "AddInt()"
    }

}

open class AllocBlock(val size: Int) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.AllocBlockExec(size)
    override fun toString(): String {
        return "AllocBlock(size=$size)"
    }

}

open class AllocStack(val maxSize: Int) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.AllocStackExec(maxSize)
    override fun toString(): String {
        return "AllocStack(maxSize=$maxSize)"
    }
}

open class Call(var routineAddress: Int? = null, val symbolicAddress: String) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.CallExec(routineAddress!!)
    override fun toString(): String {
        return "Call(routineAddress=$routineAddress, symbolicAddress='$symbolicAddress')"
    }
}

open class CondJump(val jumpAddr: Int) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.CondJumpExec(jumpAddr)
    override fun toString(): String {
        return "CondJump(jumpAddr=$jumpAddr)"
    }
}

open class Deref : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.DerefExec()
    override fun toString(): String {
        return "Deref()"
    }

}

open class DivTruncInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine): IExecInstr = vm.DivTruncIntExec()
    override fun toString(): String {
        return "DivTruncInt()"
    }
}

open class Dup : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.DupExec()
    override fun toString(): String {
        return "Dup()"
    }
}

open class EndLet(val numberOfDecls: Int) : IInstr {
    override fun toExecInstr(vm: VirtualMachine): IExecInstr = vm.EndLetExec(numberOfDecls)
    override fun toString(): String = "EndLet(numberOfDecls=$numberOfDecls)"
}

open class EqInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.EqIntExec()
    override fun toString(): String {
        return "EqInt()"
    }
}

open class GeInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.GeIntExec()
    override fun toString(): String {
        return "GeInt()"
    }
}

open class GtInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.GtIntExec()
    override fun toString(): String {
        return "GtInt()"
    }
}

open class InputBool(val indicator: String) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.InputBoolExec(indicator)
    override fun toString(): String {
        return "InputBool(indicator='$indicator')"
    }
}

open class InputInt(val indicator: String) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.InputIntExec(indicator)
    override fun toString(): String {
        return "InputInt(indicator='$indicator')"
    }
}

open class LetStore: IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.LetStoreExec()
    override fun toString(): String = "LetStore()"
}

open class LoadAddrLet(val relativeAddress: Int): IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.LoadAddrLetExec(relativeAddress)
    override fun toString(): String = "LoadAddrLet(size=$relativeAddress)"

}

open class LoadAddrRel(val relAddress: Int) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.LoadAddrRelExec(relAddress)
    override fun toString(): String {
        return "LoadAddrRel(relAddress=$relAddress)"
    }
}

open class LoadImInt(val value: Int) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.LoadImIntExec(value)
    override fun toString(): String {
        return "LoadImInt(value=$value)"
    }
}

open class LeInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.LeIntExec()
    override fun toString(): String {
        return "LeInt()"
    }
}

open class LtInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.LtIntExec()
    override fun toString(): String {
        return "LtInt()"
    }
}

open class ModTruncInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.ModTruncIntExec()
    override fun toString(): String {
        return "ModTruncInt()"
    }
}

open class MultInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.MultIntExec()
    override fun toString(): String {
        return "MultInt()"
    }
}

open class NegInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.NegIntExec()
    override fun toString(): String {
        return "NegInt()"
    }
}

open class NeInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.NeIntExec()
    override fun toString(): String {
        return "NeInt()"
    }
}

open class OutputBool(val indicator: String) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.OutputBoolExec(indicator)
    override fun toString(): String {
        return "OutputBool(indicator='$indicator')"
    }
}

open class OutputInt(val indicator: String) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.OutputIntExec(indicator)
    override fun toString(): String {
        return "OutputInt(indicator='$indicator')"
    }
}

open class Return(val size: Int) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.ReturnExec(size)
    override fun toString(): String {
        return "Return(size=$size)"
    }
}

open class SubInt : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.SubIntExec()
    override fun toString(): String {
        return "SubInt()"
    }
}

open class Stop : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.StopExec()
    override fun toString(): String {
        return "Stop()"
    }
}

open class Store : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.StoreExec()
    override fun toString(): String {
        return "Store()"
    }
}

open class UncondJump(val jumpAddr: Int) : IInstr {
    override fun toExecInstr(vm: VirtualMachine) = vm.UncondJumpExec(jumpAddr)
    override fun toString(): String {
        return "UncondJump(jumpAddr=$jumpAddr)"
    }
}
