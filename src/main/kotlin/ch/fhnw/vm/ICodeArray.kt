package ch.fhnw.vm

interface ICodeArray {

    fun put(loc: Int, instr: IInstr)

    fun resize()

    fun getSize(): Int

    fun get(loc: Int): IInstr?
}

class CodeArray: ICodeArray {
    var code = arrayOfNulls<IInstr> (1024)

    override fun put(loc: Int, instr: IInstr) {
        code[loc] = instr;
    }

    override fun resize() {
        TODO("Not yet implemented")
    }

    override fun getSize(): Int {
        TODO("Not yet implemented")
    }

    override fun get(loc: Int): IInstr? {
        return code[loc];
    }
}
