package ch.fhnw.vm

import java.lang.Exception

class ExecutionError(s: String) : Exception(s)