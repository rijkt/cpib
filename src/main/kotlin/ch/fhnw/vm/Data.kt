package ch.fhnw.vm

class Data {

    interface IBaseData

    class IntData(val i: Int): IBaseData

    class BooleanData(val boolean: Boolean): IBaseData

    companion object {

        fun intDiv(a: IBaseData?, b: IBaseData?): IntData = IntData(intGet(a) / intGet(b))

        fun boolGet(a: IBaseData?): Boolean = (a as BooleanData).boolean

        fun boolNew(a: Boolean): BooleanData = BooleanData(a)

        fun intAdd(a: IBaseData?, b: IBaseData?): IntData = IntData(intGet(a) + intGet(b))

        fun intEQ(a: IBaseData?, b: IBaseData?): BooleanData = BooleanData(intGet(a) == intGet(b))

        fun intGet(a: IBaseData?): Int = (a as IntData).i

        fun intGE(a: IBaseData?, b: IBaseData?): BooleanData = BooleanData(intGet(a) >= intGet(b))

        fun intGT(a: IBaseData?, b: IBaseData?): BooleanData = BooleanData(intGet(a) > intGet(b))

        // TODO correct?
        fun intInv(a: IBaseData?): IntData = IntData(-1 * intGet(a))

        fun intLE(a: IBaseData?, b: IBaseData?): BooleanData = BooleanData(intGet(a) <= intGet(b))

        fun intLT(a: IBaseData?, b: IBaseData?): BooleanData = BooleanData(intGet(a) < intGet(b))

        fun intMod(a: IBaseData?, b: IBaseData?): IntData = IntData(intGet(a) % intGet(b))

        fun intMult(a: IBaseData?, b: IBaseData?): IntData = IntData(intGet(a) * intGet(b))

        fun intNE(a: IBaseData?, b: IBaseData?): BooleanData = BooleanData(intGet(a) != intGet(b))

        fun intNew(a: Int): IntData = IntData(a)

        fun intSub(a: IBaseData?, b: IBaseData?): IntData = IntData(intGet(a) - intGet(b))
    }
}
