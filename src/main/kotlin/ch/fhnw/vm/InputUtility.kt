package ch.fhnw.vm

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class InputUtility {
    companion object {
        var reader: BufferedReader = BufferedReader(InputStreamReader(System.`in`))

        fun readBool(): Boolean {
            var s = ""
            try {
                s = reader.readLine();
            } catch (e: IOException) {
                throw ExecutionError("Input failed")
            }
            if (s.equals("false")) {
                return false
            } else if (s.equals("true")) {
                return true
            } else {
                throw ExecutionError("Not a boolean")
            }
        }

        fun readInt(): Int {
            var s = ""
            try {
                s = reader.readLine()
            } catch (e: IOException) {
                throw ExecutionError("Input failed")
            }
            try {
                return Integer.parseInt(s)
            } catch (e: NumberFormatException) {
                throw ExecutionError("Not an integer")
            }
        }
    }
}
