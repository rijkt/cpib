package ch.fhnw.vm

/**
 * @property pc program counter.
 * @property sp stack pointer. Grows from 0, points to first free location on the stack
 * @property ep extreme pointer (unused). Points to first always free location in stack
 * @property hp heap pointer. Points to first free location on heap. grows from store.length -1 downwards
 * @property fp frame pointer. Provides a reference to each routine incarnation
 * @property lp let pointer. Provides a reference to Let declarations
 */
class VirtualMachine(
    var instructions: Array<IInstr?>,
    var pc: Int = 0,
    var sp: Int = 0,
    var ep: Int = 0,
    var hp: Int = 0,
    var fp: Int = 0,
    var lp: Int = 0
) {

    var code: Array<IExecInstr?>

    /**
     * stores the data
     *  - stack: index 0 up to sp -1
     *  - heap: index store.length -1 down to hp + 1 (unused)
     *  numFreeStores = hp - sp + 1
     */
    var store = arrayOfNulls<Data.IBaseData?>(1024)

    init {
        code = arrayOfNulls(instructions.size)
        for (i in 0..instructions.size - 1) {
            this.code[i] = instructions.get(i)?.toExecInstr(this)
        }
    }

    fun execute() {
        hp = store.size - 1
        while (pc > -1) {
            val instruction = code[pc]
            instruction?.execute()
        }
    }

    inner class AddIntExec : AddInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intAdd(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class AllocBlockExec(size: Int) : AllocBlock(size), IExecInstr {
        override fun execute() {
            sp += size
            if (sp > hp + 1) {
                throw ExecutionError("No more storage space. sp over hp")
            }
            pc += 1
        }
    }

    inner class AllocStackExec(maxSize: Int) : AllocStack(maxSize), IExecInstr {
        override fun execute() {
            ep = sp + maxSize
            if (ep > hp + 1) {
                throw ExecutionError("ep over hp")
            }
            pc += 1
        }
    }

    inner class CallExec(routineAddress: Int) : Call(routineAddress = routineAddress, "unused"), IExecInstr {
        override fun execute() {
            if (sp + 2 > hp) {
                throw ExecutionError("sp over hp")
            }
            store[sp] = Data.intNew(fp)
            store[sp + 1] = Data.intNew(ep)
            store[sp + 2] = Data.intNew(pc)
            fp = sp
            sp = sp + 3
            pc = routineAddress !!
        }
    }

    inner class CondJumpExec(jumpAddr: Int) : CondJump(jumpAddr), IExecInstr {
        override fun execute() {
            sp -= 1
            if (Data.boolGet(store[sp])) {
                pc += 1
            } else {
                pc = jumpAddr
            }
        }
    }

    inner class DerefExec : Deref(), IExecInstr {
        override fun execute() {
            val address = Data.intGet(store[sp - 1])
            store[sp - 1] = store[address]
            pc += 1
        }
    }

    inner class DivTruncIntExec : DivTruncInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp-1] = Data.intDiv(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class DupExec : Dup(), IExecInstr {
        override fun execute() {
            if (sp > hp) {
                throw ExecutionError("sp over hp")
            }
            store[sp] = store[sp - 1] // TODO: change to store[sp] = store[sp - 1].copy()
            sp += 1
            pc += 1
        }
    }

    /**
     * Top of stack is empty, sp == lp
     */
    inner class EndLetExec(numberOfDecls: Int): EndLet(numberOfDecls), IExecInstr {
        override fun execute() {
            val lpIsTopOfStack = lp + 1 != sp
            if (lpIsTopOfStack) throw ExecutionError("sp must be over lp.")
            lp = Data.intGet(store[lp]) // load old lp
            sp -= numberOfDecls + 1
            if (sp > hp + 1) {
                throw ExecutionError("No more storage space. sp over hp")
            }
            pc += 1
        }
    }

    inner class EqIntExec : EqInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intEQ(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class GeIntExec : GeInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intGE(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class GtIntExec : GtInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intGT(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class InputBoolExec(indicator: String) : InputBool(indicator), IExecInstr {
        override fun execute() {
             val input = InputUtility.readBool() // comment to run tests
//            val input = false // uncomment to run tests
            val address = Data.intGet(store[sp - 1])
            store[address] = Data.boolNew(input)
            sp -= 1
            pc += 1
        }
    }

    inner class InputIntExec(indicator: String) : InputInt(indicator), IExecInstr {
        override fun execute() {
             val input = InputUtility.readInt() // comment to run tests
//            val input = 0 // uncomment to run tests
            val address = Data.intGet(store[sp - 1])
            store[address] = Data.intNew(input)
            sp -= 1
            pc += 1
        }
    }

    /**
     * @property size call after AllocBlock with same size
     */
    inner class LetStoreExec: LetStore(), IExecInstr {
        override fun execute() {
            store[sp] = Data.intNew(lp) // save old lp
            lp = sp // point to previous lp
            sp++ // move to start of let cmds
            pc++
        }
    }

    inner class LoadAddrLetExec(relativeAddress: Int): LoadAddrLet(relativeAddress), IExecInstr {
        override fun execute() {
            if (sp > hp) {
                throw ExecutionError("sp over hp")
            }
            if (relativeAddress >= 0) throw AssertionError("Wrong let address: $relativeAddress")
            store[sp] = Data.intNew(lp + relativeAddress)
            sp += 1
            pc += 1
        }
    }

    inner class LoadAddrRelExec(relAdress: Int) : LoadAddrRel(relAdress), IExecInstr {
        override fun execute() {
            if (sp > hp) {
                throw ExecutionError("sp over hp")
            }
            store[sp] = Data.intNew(fp + relAddress)
            sp += 1
            pc += 1
        }
    }

    inner class LoadImIntExec(value: Int) : LoadImInt(value), IExecInstr {
        override fun execute() {
            if (sp > hp) {
                throw ExecutionError("sp over hp")
            }
            store[sp] = Data.IntData(value)
            sp += 1
            pc += 1
        }
    }

    inner class LeIntExec : LeInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intLE(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class LtIntExec : LtInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intLT(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class ModTruncIntExec : ModTruncInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intMod(store[sp], store[sp-1])
            pc += 1
        }
    }

    inner class MultIntExec : MultInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intMult(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class NegIntExec : NegInt(), IExecInstr {
        override fun execute() {
            store[sp - 1] = Data.intInv(store[sp - 1])
            pc += 1
        }
    }

    inner class NeIntExec : NeInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intNE(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class OutputBoolExec(indicator: String) : OutputBool(indicator), IExecInstr {
        override fun execute() {
            sp -= 1
            val output = Data.boolGet(store[sp])
            println("! " + indicator + " : bool = " + output)
            pc += 1
        }
    }

    inner class OutputIntExec(indicator: String) : OutputInt(indicator), IExecInstr {
        override fun execute() {
            sp -= 1
            val output = Data.intGet(store[sp])
            println("! " + indicator + " : int = " + output)
            pc += 1
        }
    }

    inner class ReturnExec(size: Int) : Return(size), IExecInstr {
        override fun execute() {
            sp = fp - size
            pc = Data.intGet(store[fp + 2]) + 1
            ep = Data.intGet(store[fp + 1])
            fp = Data.intGet(store[fp])
            if (ep > hp + 1) {
                throw ExecutionError("ep over hp")
            }
        }
    }

    inner class SubIntExec : SubInt(), IExecInstr {
        override fun execute() {
            sp -= 1
            store[sp - 1] = Data.intSub(store[sp - 1], store[sp])
            pc += 1
        }
    }

    inner class StopExec : Stop(), IExecInstr {
        override fun execute() {
            pc = -1
        }
    }

    inner class StoreExec : Store(), IExecInstr {
        override fun execute() {
            val address = Data.intGet(store[sp - 2])
            store[address] = store[sp - 1]
            sp -= 2
            pc += 1
        }
    }

    inner class UncondJumpExec(jumpAddr: Int) : UncondJump(jumpAddr), IExecInstr {
        override fun execute() {
            pc = jumpAddr
        }
    }
}
