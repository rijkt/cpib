package ch.fhnw

import ch.fhnw.generator.CodeGenerator
import ch.fhnw.parser.Parser
import ch.fhnw.scanner.scan
import ch.fhnw.vm.Call
import ch.fhnw.vm.Code
import ch.fhnw.vm.VirtualMachine
import java.io.File

fun main(args: Array<String>) {
    val fileName = args[0]
    println("Read file $fileName")
    val input = File(fileName).readText()
    val tokenList = scan(input)
    tokenList.map { println(it) }
    val parser = Parser(tokenList)
    val concreteTree = parser.parse()
    val abstractTree = concreteTree.toAbsSyn()
    println()
    println("Parsed AST:")
    println(abstractTree)
    abstractTree.check()
    val codeGenerator = CodeGenerator()
    codeGenerator.code(abstractTree, 0)
    println()
    println("Generated instructions:")
    var line = 0
    Code.codeArray.code.map {
        if (it != null) {
            // symbolic translation
            if (it::class == Call::class) {
                val call = it as Call
                val routine = abstractTree.ident.environment.getRoutine(call.symbolicAddress)
                call.routineAddress = routine!!.address
            }
            println("$line: $it")
            line++
        }
    }
    println("Execution output:")
    val vm = VirtualMachine(Code.codeArray.code)
     vm.execute() // comment to run tests
}
