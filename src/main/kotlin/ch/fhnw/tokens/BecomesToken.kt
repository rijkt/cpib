package ch.fhnw.tokens

import ch.fhnw.Terminal

data class BecomesToken(val becomes: Terminal = Terminal.BECOMES): Token {
    override val terminal: Terminal get() = Terminal.BECOMES
}
