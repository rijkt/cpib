package ch.fhnw.tokens

import ch.fhnw.Terminal

data  class ColonToken(val colon: Terminal = Terminal.COLON): Token {
    override val terminal: Terminal get() = Terminal.COLON
}
