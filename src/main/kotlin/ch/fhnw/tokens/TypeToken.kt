package ch.fhnw.tokens

import ch.fhnw.Terminal
import ch.fhnw.tokens.groups.Type

data class TypeToken(val type: Type): Token {
    override val terminal: Terminal get() = Terminal.TYPE
}