package ch.fhnw.tokens

import ch.fhnw.Terminal

data class InitToken(private val init: Terminal = Terminal.INIT) : Token {
    override val terminal: Terminal get() = init
}
