package ch.fhnw.tokens

import ch.fhnw.Terminal

data class DebugOutToken(private val debugout: Terminal = Terminal.DEBUGOUT) : Token {
    override val terminal: Terminal get() = debugout
}
