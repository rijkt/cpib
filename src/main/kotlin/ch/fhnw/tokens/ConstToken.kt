package ch.fhnw.tokens

import ch.fhnw.Terminal

data class ConstToken(private val const: Terminal = Terminal.CONST) : Token {
    override val terminal: Terminal get() = const
}
