package ch.fhnw.tokens

import ch.fhnw.Terminal

data class WhileToken(val `while`: Terminal = Terminal.WHILE) : Token {
    override val terminal: Terminal get() = Terminal.WHILE
}
