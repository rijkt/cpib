package ch.fhnw.tokens

import ch.fhnw.Terminal

data class ProgramToken(private val program: Terminal = Terminal.PROGRAM) : Token {
    override val terminal: Terminal get() = this.program
}
