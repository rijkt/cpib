package ch.fhnw.tokens

import ch.fhnw.Terminal

data class CallToken(private val call: Terminal = Terminal.CALL) : Token {
    override val terminal: Terminal get() = call
}
