package ch.fhnw.tokens

import ch.fhnw.Terminal

data class EndIfToken(private val endIf: Terminal = Terminal.ENDIF) : Token {
    override val terminal: Terminal get() = this.endIf
}
