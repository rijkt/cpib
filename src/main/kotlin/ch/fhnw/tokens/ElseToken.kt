package ch.fhnw.tokens

import ch.fhnw.Terminal

data class ElseToken(private val `else`: Terminal = Terminal.ELSE) : Token {
    override val terminal: Terminal get() = this.`else`
}
