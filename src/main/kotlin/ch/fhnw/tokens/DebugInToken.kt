package ch.fhnw.tokens

import ch.fhnw.Terminal

data class DebugInToken(private val debugin: Terminal = Terminal.DEBUGIN) : Token {
    override val terminal: Terminal get() = debugin
}
