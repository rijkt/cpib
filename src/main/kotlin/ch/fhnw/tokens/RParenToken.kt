package ch.fhnw.tokens

import ch.fhnw.Terminal

data class RParenToken(val rparen: Terminal = Terminal.RPAREN): Token {
    override val terminal: Terminal get() = Terminal.RPAREN
}