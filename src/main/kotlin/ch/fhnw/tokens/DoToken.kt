package ch.fhnw.tokens

import ch.fhnw.Terminal

data class DoToken(val `do`: Terminal = Terminal.DO) : Token {
    override val terminal: Terminal get() = Terminal.DO
}
