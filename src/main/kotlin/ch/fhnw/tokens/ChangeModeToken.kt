package ch.fhnw.tokens

import ch.fhnw.Terminal
import ch.fhnw.tokens.groups.ChangeMode

data class ChangeModeToken(val mode: ChangeMode) : Token {
    override val terminal: Terminal get() = Terminal.CHANGEMODE
}
