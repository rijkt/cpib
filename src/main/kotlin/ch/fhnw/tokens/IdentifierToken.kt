package ch.fhnw.tokens

import ch.fhnw.Terminal

data class IdentifierToken(val name: String): Token {
    override val terminal: Terminal get() = Terminal.IDENT
}