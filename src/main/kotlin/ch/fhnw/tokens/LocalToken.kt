package ch.fhnw.tokens

import ch.fhnw.Terminal

data class LocalToken(private val local: Terminal = Terminal.LOCAL) : Token {
    override val terminal: Terminal get() = local
}
