package ch.fhnw.tokens

import ch.fhnw.Terminal

data class LParenToken(val lparen: Terminal = Terminal.LPAREN): Token {
    override val terminal: Terminal get() = Terminal.LPAREN
}