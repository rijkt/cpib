package ch.fhnw.tokens

import ch.fhnw.Terminal
import ch.fhnw.tokens.groups.MechMode

data class MechModeToken(val mechMode: MechMode) : Token {
    override val terminal: Terminal get() = Terminal.MECHMODE
}
