package ch.fhnw.tokens

import ch.fhnw.Terminal

data class EndLetToken(val endLet: Terminal = Terminal.ENDLET) : Token {
    override val terminal: Terminal get() = Terminal.ENDLET
}
