package ch.fhnw.tokens

import ch.fhnw.Terminal

data class EndFunToken(private val endFun: Terminal = Terminal.ENDFUN) : Token {
    override val terminal: Terminal get() = this.endFun
}
