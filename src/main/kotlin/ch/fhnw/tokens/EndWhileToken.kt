package ch.fhnw.tokens

import ch.fhnw.Terminal

data class EndWhileToken(val endWhile: Terminal = Terminal.ENDWHILE) : Token {
    override val terminal: Terminal get() = Terminal.ENDWHILE
}
