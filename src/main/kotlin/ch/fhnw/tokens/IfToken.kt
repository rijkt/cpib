package ch.fhnw.tokens

import ch.fhnw.Terminal

data class IfToken(private val `if`: Terminal = Terminal.IF) : Token {
    override val terminal: Terminal get() = `if`
}
