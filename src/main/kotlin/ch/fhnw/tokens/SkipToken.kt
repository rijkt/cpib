package ch.fhnw.tokens

import ch.fhnw.Terminal

data class SkipToken(private val skip: Terminal = Terminal.SKIP) : Token {
    override val terminal: Terminal get() = this.skip
}
