package ch.fhnw.tokens

import ch.fhnw.Terminal

data class FunToken(private val `fun`: Terminal = Terminal.FUN) : Token {
    override val terminal: Terminal get() = this.`fun`
}
