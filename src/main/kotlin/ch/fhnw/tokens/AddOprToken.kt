package ch.fhnw.tokens

import ch.fhnw.Terminal
import ch.fhnw.tokens.groups.Operator

data class AddOprToken(val operator: Operator): Token {
    override val terminal: Terminal get() = Terminal.ADDOPR
}