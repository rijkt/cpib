package ch.fhnw.tokens

import ch.fhnw.Terminal
import ch.fhnw.tokens.groups.FlowMode

data class FlowToken(val flowMode: FlowMode) : Token {
    override val terminal: Terminal get() = Terminal.FLOWMODE
}
