package ch.fhnw.tokens

import ch.fhnw.Terminal

data class CommaToken(val comma: Terminal = Terminal.COMMA): Token {
    override val terminal: Terminal get() = Terminal.COMMA
}