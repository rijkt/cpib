package ch.fhnw.tokens.groups

enum class Type {
    BOOL,
    INT1024,
    INT32,
    INT64,
    VOID // for things like assignments, etc.
}
