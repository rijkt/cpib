package ch.fhnw.tokens.groups

enum class FlowMode {
    IN,
    INOUT,
    OUT
}
