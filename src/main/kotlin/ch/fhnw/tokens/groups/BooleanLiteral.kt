package ch.fhnw.tokens.groups

enum class BooleanLiteral {
    FALSE,
    TRUE
}
