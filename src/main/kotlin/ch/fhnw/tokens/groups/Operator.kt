package ch.fhnw.tokens.groups

enum class Operator {
    CAND,
    COR,
    DIV_E,
    DIV_F,
    DIV_T,
    EQ,
    GE,
    GT,
    LE,
    LT,
    MINUS,
    MOD_E,
    MOD_F,
    MOD_T,
    NE,
    NOT,
    PLUS,
    TIMES
}