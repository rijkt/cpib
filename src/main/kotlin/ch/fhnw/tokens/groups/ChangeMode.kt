package ch.fhnw.tokens.groups

enum class ChangeMode {
    CONST,
    VAR
}
