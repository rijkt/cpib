package ch.fhnw.tokens.groups

enum class MechMode {
    COPY,
    REF
}
