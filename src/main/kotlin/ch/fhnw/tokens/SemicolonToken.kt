package ch.fhnw.tokens

import ch.fhnw.Terminal

data class SemicolonToken(val comma: Terminal = Terminal.SEMICOLON): Token {
    override val terminal: Terminal get() = Terminal.SEMICOLON
}