package ch.fhnw.tokens

import ch.fhnw.Terminal

data class ThenToken(private val then: Terminal = Terminal.THEN) : Token {
    override val terminal: Terminal get() = this.then
}
