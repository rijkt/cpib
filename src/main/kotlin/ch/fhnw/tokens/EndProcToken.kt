package ch.fhnw.tokens

import ch.fhnw.Terminal

data class EndProcToken(private val endProc: Terminal = Terminal.ENDPROC) : Token {
    override val terminal: Terminal get() = this.endProc
}
