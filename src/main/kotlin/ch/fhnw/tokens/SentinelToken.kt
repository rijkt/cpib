package ch.fhnw.tokens

import ch.fhnw.Terminal

data class SentinelToken(val sentinel: Terminal = Terminal.SENTINEL): Token {
    override val terminal: Terminal get() = Terminal.SENTINEL
}
