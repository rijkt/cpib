package ch.fhnw.tokens

import ch.fhnw.Terminal

data class NotToken(private val not: Terminal = Terminal.NOTOPR) : Token {
    override val terminal: Terminal get() = not
}
