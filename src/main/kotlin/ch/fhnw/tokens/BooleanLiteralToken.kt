package ch.fhnw.tokens

import ch.fhnw.Terminal
import ch.fhnw.tokens.groups.BooleanLiteral

data class BooleanLiteralToken(val booleanLiteral: BooleanLiteral) : Token {
    override val terminal: Terminal get() = Terminal.LITERAL
}
