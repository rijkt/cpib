package ch.fhnw.tokens

import ch.fhnw.Terminal

// can't be compared in list
interface Token {
    val terminal: Terminal
}
