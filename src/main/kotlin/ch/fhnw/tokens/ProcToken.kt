package ch.fhnw.tokens

import ch.fhnw.Terminal

data class ProcToken(private val proc: Terminal = Terminal.PROC) : Token {
    override val terminal: Terminal get() = this.proc
}
