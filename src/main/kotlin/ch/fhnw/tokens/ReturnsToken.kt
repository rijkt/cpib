package ch.fhnw.tokens

import ch.fhnw.Terminal

data class ReturnsToken(private val returns: Terminal = Terminal.RETURNS) : Token {
    override val terminal: Terminal get() = this.returns
}
