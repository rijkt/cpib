package ch.fhnw.tokens

import ch.fhnw.Terminal

data class LiteralToken(val value: Int): Token {
    override val terminal: Terminal get() = Terminal.LITERAL
}