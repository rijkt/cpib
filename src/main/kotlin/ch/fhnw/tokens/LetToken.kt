package ch.fhnw.tokens

import ch.fhnw.Terminal

data class LetToken(val let: Terminal = Terminal.LET) : Token {
    override val terminal: Terminal get() = Terminal.LET
}
