package ch.fhnw.tokens

import ch.fhnw.Terminal

data class EndProgramToken(private val endProgram: Terminal = Terminal.ENDPROGRAM) : Token {
    override val terminal: Terminal get() = this.endProgram
}
