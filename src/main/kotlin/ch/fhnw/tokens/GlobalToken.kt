package ch.fhnw.tokens

import ch.fhnw.Terminal

data class GlobalToken(private val global: Terminal = Terminal.GLOBAL) : Token {
    override val terminal: Terminal get() = global
}
