package ch.fhnw.tokens

import ch.fhnw.Terminal

data class ExclamationMarkToken(val exclamationMark: Terminal = Terminal.EXCLAMATIONMARK): Token {
    override val terminal: Terminal get() = Terminal.EXCLAMATIONMARK
}