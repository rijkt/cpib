package ch.fhnw.scanner

import ch.fhnw.tokens.*
import ch.fhnw.tokens.groups.Operator

fun scan(input: String): List<Token> {
    assert(input.isNotEmpty())
    assert(input[input.length - 1] == '\n') // otherwise identifiers might not be finished
    var state = ScanningState.INITIAL
    var lexemeAccumulator = ""
    var numberAccumulator: Long = 0
    val tokenList = mutableListOf<Token>()
    var i = 0
    while (i < input.length) { // need to manipulate index
        val c = input[i]
        when (state) {
            ScanningState.INITIAL -> {
                when {
                    isLetter(c) -> {
                        // identifier, reserved identifier or operator
                        state = ScanningState.IDENTIFIER
                        lexemeAccumulator = c.toString()
                    }
                    c.isWhitespace() -> state = ScanningState.INITIAL // just skip it.
                    c.isDigit() -> {
                        state = ScanningState.LITERAL
                        numberAccumulator = getLong(c)
                    }
                    c == '<' -> state = ScanningState.LESS // lexemeAcc not used, since it's at most 2 chars long
                    c == '>' -> state = ScanningState.GREATER // lexemeAcc not used, since it's at most 2 chars long
                    c == ':' -> state = ScanningState.COLON // same as above
                    c == '*' -> tokenList.add(MultOprToken(Operator.TIMES))
                    c == '+' -> tokenList.add(AddOprToken(Operator.PLUS))
                    c == '-' -> tokenList.add(AddOprToken(Operator.MINUS))
                    c == '!' -> tokenList.add(ExclamationMarkToken())
                    c == '/' -> {
                        state = ScanningState.FORWARD_SLASH
                        lexemeAccumulator = c.toString()
                    }
                    c == '\\' -> {
                        state = ScanningState.BACK_SLASH
                        lexemeAccumulator = c.toString()
                    }
                    c == '=' -> tokenList.add(RelOprToken(Operator.EQ))
                    c == '(' -> tokenList.add(LParenToken())
                    c == ')' -> tokenList.add(RParenToken())
                    c == ',' -> tokenList.add(CommaToken())
                    c == ';' -> tokenList.add(SemicolonToken())
                    else -> throw LexicalError(c)
                }
            }
            ScanningState.IDENTIFIER -> { // todo: naming
                if (isLetter(c) || c.isDigit() || c == '_' || c == '\'') {
                    lexemeAccumulator += c
                } else {
                    // differentiate between identifier or keyword
                    var reserved = false
                    for (identifier in ReservedIdentifier.values()) {
                        if (identifier.toString() == lexemeAccumulator.toUpperCase()) {
                            tokenList.add(identifier.token)
                            reserved = true
                        }
                    }
                    state = ScanningState.INITIAL
                    i-- // one back for next lexeme
                    if(!reserved) {
                        tokenList.add(IdentifierToken(lexemeAccumulator))
                    }
                }
            }
            ScanningState.LITERAL -> {
                if (c.isDigit()) {
                    state = ScanningState.LITERAL
                    numberAccumulator = numberAccumulator * 10 + getLong(c)
                    if (numberAccumulator > Integer.MAX_VALUE) {
                        throw LexicalError("Integer literal too large!")
                    }
                } else {
                    state = ScanningState.INITIAL
                    i-- // one back for next lexeme
                    tokenList.add(LiteralToken(numberAccumulator.toInt()))
                }
            }
            ScanningState.LESS -> {
                if (c == '=') {
                    state = ScanningState.INITIAL
                    tokenList.add(RelOprToken(Operator.LE))
                } else {
                    state = ScanningState.INITIAL
                    i-- // one back for next lexeme
                    tokenList.add(RelOprToken(Operator.LT))
                }
            }
            ScanningState.GREATER -> {
                if (c == '=') {
                    state = ScanningState.INITIAL
                    tokenList.add(RelOprToken(Operator.GE))
                } else {
                    state = ScanningState.INITIAL
                    i-- // one back for next lexeme
                    tokenList.add(RelOprToken(Operator.GT))
                }
            }
            ScanningState.COLON -> {
                if (c == '=') {
                    state = ScanningState.INITIAL
                    tokenList.add(BecomesToken())
                } else {
                    state = ScanningState.INITIAL
                    i-- // one back for next lexeme
                    tokenList.add(ColonToken())
                }
            }
            ScanningState.FORWARD_SLASH -> {
                if (c == '=') {
                    state = ScanningState.INITIAL
                    tokenList.add(RelOprToken(Operator.NE))
                } else if (c == '\\') {
                    state = ScanningState.CAND
                    lexemeAccumulator = c.toString()
                } else {
                    throw LexicalError(c)
                }
            }
            ScanningState.CAND -> {
                if (c == '?') {
                    state = ScanningState.INITIAL
                    tokenList.add(BoolOprToken(Operator.CAND))
                } else {
                    throw LexicalError(c)
                }
            }
            ScanningState.BACK_SLASH -> {
                if (c == '/') {
                    state = ScanningState.COR
                    lexemeAccumulator = c.toString()
                } else {
                    throw LexicalError(c)
                }
            }
            ScanningState.COR -> {
                if (c == '?') {
                    state = ScanningState.INITIAL
                    tokenList.add(BoolOprToken(Operator.COR))
                } else {
                    throw LexicalError(c)
                }
            }
        }
        i++ // increment in every case, was for loop in slides
    }
    assert(state == ScanningState.INITIAL)
    tokenList.add(SentinelToken())
    return tokenList
}

private fun getLong(c: Char) = c.toString().toLong()

private fun isLetter(c: Char) = Regex("[a-z]|[A-Z]").matches(c.toString())
