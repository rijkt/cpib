package ch.fhnw.scanner

import ch.fhnw.tokens.*
import ch.fhnw.tokens.groups.*

/**
 * Constants represent keywords in uppercase
 */
enum class ReservedIdentifier(val token: Token) {
    BOOL(TypeToken(Type.BOOL)),
    CALL(CallToken()),
    CONST(ChangeModeToken(ChangeMode.CONST)),
    COPY(MechModeToken(MechMode.COPY)),
    DEBUGIN(DebugInToken()),
    DEBUGOUT(DebugOutToken()),
    DIVE(MultOprToken(Operator.DIV_E)),
    DIVF(MultOprToken(Operator.DIV_F)),
    DIVT(MultOprToken(Operator.DIV_T)),
    DO(DoToken()),
    ELSE(ElseToken()),
    ENDFUN(EndFunToken()),
    ENDIF(EndIfToken()),
    ENDLET(EndLetToken()),
    ENDPROC(EndProcToken()),
    ENDPROGRAM(EndProgramToken()),
    ENDWHILE(EndWhileToken()),
    FALSE(BooleanLiteralToken(BooleanLiteral.FALSE)),
    FUN(FunToken()),
    GLOBAL(GlobalToken()),
    IF(IfToken()),
    IN(FlowToken(FlowMode.IN)),
    INIT(InitToken()),
    INOUT(FlowToken(FlowMode.INOUT)),
    INT1024(TypeToken(Type.INT1024)),
    INT32(TypeToken(Type.INT32)),
    INT64(TypeToken(Type.INT64)),
    LET(LetToken()),
    LOCAL(LocalToken()),
    MODE(MultOprToken(Operator.MOD_E)),
    MODF(MultOprToken(Operator.MOD_F)),
    MODT(MultOprToken(Operator.MOD_T)),
    NOT(NotToken()),
    OUT(FlowToken(FlowMode.OUT)),
    PROC(ProcToken()),
    PROGRAM(ProgramToken()),
    REF(MechModeToken(MechMode.REF)),
    RETURNS(ReturnsToken()),
    SKIP(SkipToken()),
    THEN(ThenToken()),
    TRUE(BooleanLiteralToken(BooleanLiteral.TRUE)),
    VAR(ChangeModeToken(ChangeMode.VAR)),
    WHILE(WhileToken())
}