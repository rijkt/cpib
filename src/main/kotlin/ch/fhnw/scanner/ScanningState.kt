package ch.fhnw.scanner

enum class ScanningState {
    BACK_SLASH, // operator COR
    CAND, // CAND symbol
    COLON, // assignments and type declarations
    COR, // COR symbol
    FORWARD_SLASH, // operators (CAND or NE)
    GREATER, // operators
    IDENTIFIER, // starts with letter, can have digits, underlines and apostrophe (1 in script)
    INITIAL, // no character read (0 in script)
    LESS, // operators
    LITERAL, // boolean literal or int literal (2 in script)
}