package ch.fhnw.scanner

class LexicalError(message: String) : Exception(message) {
    constructor(c: Char) : this("Error at character $c")
}