package ch.fhnw.parser

import java.lang.Exception

class GrammarError(s: String) : Exception(s)
