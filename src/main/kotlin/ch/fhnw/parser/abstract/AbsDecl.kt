package ch.fhnw.parser.abstract

import ch.fhnw.symboltable.DeclarationLocation
import ch.fhnw.tokens.groups.ChangeMode
import ch.fhnw.tokens.groups.Type
import ch.fhnw.vm.AllocBlock
import ch.fhnw.vm.Code.Companion.codeArray
import ch.fhnw.vm.Return

interface IAbsDecl : IAbsNode, IAbsCodeGeneration

data class AbsStoDecl(val changeMode: ChangeMode?, val type: Type, val ident: String) : IAbsDecl {
    override fun check(): Type = type

    override fun code(loc: Int): Int {
        val loc1 = loc
        codeArray.put(loc, AllocBlock(1))
        return loc1 + 1
    }
}

data class AbsFunDecl(
    val ident: AbsIdentifier,
    val absParams: List<AbsParam>?,
    val returns: AbsStoDecl,
    val absGlobImps: List<AbsGlobImp>?,
    val absOptLocalCpsStoDecls: List<AbsStoDecl>?,
    val absCmds: List<IAbsCmd>
) : IAbsDecl {
    override fun check(): Type {
        absCmds.forEach { it.check() }
        return Type.VOID
    }

    /**
     * Preconditions at runtime:
     * Directly before sp: 3 organizational cells.
     * fp points to previous fp in organizational cell 1.
     * Cell 3 holds the previous pc.
     * pc points to first AllocBlock generated here
     * Before the organizational cells: variables.
     * Before those, the r-value for the return value.
     */
    override fun code(loc: Int): Int {
        var loc1 = loc
        val environment = ident.environment
        val locals = environment.vars.values.stream()
                .filter { it.declarationLocation == DeclarationLocation.LOCAL }
                .count().toInt()
        codeArray.put(loc1, AllocBlock(locals))
        loc1 = returns.code(loc1)
        for (c in absCmds) {
            loc1 = c.code(loc1)
        }
        var paramCount = 0
        environment.vars.values.forEach {
            if (it.declarationLocation == DeclarationLocation.PARAM) {
                // todo: only copy and ref params
                paramCount++
            }
        }
        codeArray.put(loc1, Return(paramCount))
        return loc1 + 1
    }
}

data class AbsProcDecl(
    val ident: AbsIdentifier,
    val absParams: List<AbsParam>?,
    val absGlobImps: List<AbsGlobImp>?,
    val absStoDecls: List<AbsStoDecl>?,
    val absCmds: List<IAbsCmd>
) : IAbsDecl {
    override fun check(): Type {
        absCmds.forEach { it.check() }
        return Type.VOID
    }

    override fun code(loc: Int): Int {
        var loc1 = loc
        val environment = ident.environment
        val locals = environment.vars.values.stream()
                .filter { it.declarationLocation == DeclarationLocation.LOCAL }
                .count().toInt()
        codeArray.put(loc1, AllocBlock(locals))
        loc1++
        for (c in absCmds) {
            loc1 = c.code(loc1)
        }
        var paramCount = 0
        environment.vars.values.forEach {
            if (it.declarationLocation == DeclarationLocation.PARAM) {
                // todo: only copy and ref params
                paramCount++
            }
        }
        codeArray.put(loc1, Return(paramCount))
        return loc1 + 1
    }
}
