package ch.fhnw.parser.abstract

import ch.fhnw.checker.LvalueError
import ch.fhnw.checker.TypeError
import ch.fhnw.tokens.groups.Type
import ch.fhnw.vm.*
import ch.fhnw.vm.Code.Companion.codeArray

interface IAbsCmd: IAbsNode, IAbsCodeGeneration

data class AbsAssiCmd(private val expr1: IAbsExpr, private val expr2: IAbsExpr): IAbsCmd {
    override fun check(): Type {
        val type1 = expr1.check()
        val type2 = expr2.check()
        if(!expr1.lvalue()) throw LvalueError("Expression $expr1 is not an l value")
        val storeExpr = expr1 as AbsStoreExpr
        storeExpr.write(true)
        if (type1 == type2) {
            return Type.VOID
        } else {
            throw TypeError("Type error in assignment: Types do not match: expr1: $type1 $expr1, expr2: $type2 $expr2")
        }
    }

    override fun code(loc: Int): Int {
        val loc1 = loc
        val loc2 = expr1.code(loc1)
        val loc3 = expr2.code(loc2)
        codeArray.put(loc3, Store())
        return loc3 + 1
    }
}

data class AbsCondCmd(private val expr: IAbsExpr, private val  thenCmd: List<IAbsCmd>, private val elseCmd: List<IAbsCmd>?) : IAbsCmd {
    override fun check(): Type {
        val guardType = expr.check()
        if(guardType === Type.BOOL) {
            thenCmd.forEach { it.check() }
            elseCmd?.forEach { it.check() }
            return Type.VOID
        } else {
            throw TypeError("Type error in if cmd: expected guard type bool, but got $guardType")
        }
    }

    override fun code(loc: Int): Int {
        val booleanLocation = expr.code(loc)
        var thenLocation = booleanLocation + 1
        for (c in thenCmd) {
            thenLocation = c.code(thenLocation)
        }
        var elseLocation = thenLocation + 1 // UndcondJump after then
        codeArray.put(booleanLocation, CondJump(elseLocation))
        if (elseCmd != null) {
            for (c in elseCmd) {
                elseLocation = c.code(elseLocation)
            }
        }
        codeArray.put(thenLocation, UncondJump(elseLocation))
        return elseLocation
    }
}

data class AbsInputCmd(private val expr: IAbsExpr) : IAbsCmd {
    override fun check(): Type {
        // store can only be checked at runtime
        expr.check()
        val storeExpr = expr as AbsStoreExpr
        storeExpr.write(true)
        return Type.VOID
    }

    override fun code(loc: Int): Int {
        val loc1 = expr.code(loc)
        val type = expr.check()
        when (type) {
             Type.BOOL -> {
                 codeArray.put(loc1, InputBool("Bool"))
             }
             Type.INT1024 -> {
                 codeArray.put(loc1, InputInt("Int"))
             }
             else -> throw ExecutionError("type ${type} not supported in vm")
         }
        return loc1 + 1
    }
}

data class AbsLetCmd(private val stoDecl: List<AbsStoDecl>, private val  cmd: List<IAbsCmd>) : IAbsCmd {
    override fun check(): Type {
        cmd.forEach { it.check()}
        return Type.VOID
    }

    override fun code(loc: Int): Int {
        val numberOfDecls = stoDecl.size
        codeArray.put(loc, AllocBlock(numberOfDecls))
        val loc2 = loc + 1
        codeArray.put(loc2, LetStore())
        var cmdLocation = loc2 + 1
        for (c in cmd) {
            cmdLocation = c.code(cmdLocation)
        }
        val endLetLocation = cmdLocation
        codeArray.put(endLetLocation, EndLet(numberOfDecls))
        return endLetLocation + 1
    }
}

data class AbsOutputCmd(private val expr: IAbsExpr) : IAbsCmd {
    override fun check(): Type {
        expr.check()
        return Type.VOID
    }

    override fun code(loc: Int): Int {
        val loc1 = expr.code(loc)
        val type = expr.check()
        when (type) {
            Type.BOOL -> {
                codeArray.put(loc1, OutputBool("Bool"))
            }
            Type.INT1024 -> {
                codeArray.put(loc1, OutputInt("Int"))
            }
            else -> throw ExecutionError("type ${type} not supported in vm")
        }
        return loc1 + 1
    }
}

data class AbsProcCmd(private val routineCall: RoutineCall, private val globInits: List<String>) : IAbsCmd {
    override fun check(): Type {
        routineCall.check()
        // do something with globInits?
        return Type.VOID
    }

    override fun code(loc: Int): Int {
        val loc1 = loc
        // TODO how to handle globInits?
        val loc2 = routineCall.code(loc1)
        return loc2
    }
}

class AbsSkipCmd: IAbsCmd {
    override fun check(): Type {
        // ?
        return Type.VOID
    }

    override fun code(loc: Int): Int {
        return loc
    }
}

data class AbsWhileCmd(private val expr: IAbsExpr, private val  cmd: List<IAbsCmd>) : IAbsCmd {
    override fun check(): Type {
        val guardType = expr.check()
        if (guardType === Type.BOOL) {
            cmd.forEach { it.check() }
            return Type.VOID
        } else {
            throw TypeError("Type error in while cmd: expected guard type bool, but got $guardType")
        }
    }

    override fun code(loc: Int): Int {
        val loc1 = expr.code(loc)
        val loc2 = loc1 + 1
        var loc3 = loc2
        for (c in cmd) {
            loc3 = c.code(loc3)
        }
        val loc4 = loc3 + 1
        codeArray.put(loc1, CondJump(loc4))
        codeArray.put(loc3, UncondJump(loc))
        return loc4
    }
}
