package ch.fhnw.parser.abstract

import ch.fhnw.tokens.groups.Type

interface IAbsTypedIdent

data class AbsTypedIdent(val ident: String, val type: Type) : IAbsTypedIdent