package ch.fhnw.parser.abstract

import ch.fhnw.checker.UndeclaredIdentifierError
import ch.fhnw.tokens.groups.Type
import ch.fhnw.vm.AllocBlock
import ch.fhnw.vm.Call
import ch.fhnw.vm.Code.Companion.codeArray

data class RoutineCall(val ident: AbsIdentifier, val expressions: List<IAbsExpr>) : IAbsNode, IAbsCodeGeneration {
    override fun check(): Type {
        expressions.forEach { it.check() }
        val routine = ident.environment.getRoutine(ident.name)
                ?: throw UndeclaredIdentifierError("Routine ${ident.name} was not declared")
        return routine.returnType
    }

    override fun code(loc: Int): Int {
        val loc1 = loc
        val routine = ident.environment.getRoutine(ident.name)
                ?: throw AssertionError("Null routine in code generation: $ident.name")
        val isFunction = routine.returnType != Type.VOID
        val size = if (isFunction) 1 else 0
        codeArray.put(loc1, AllocBlock(size)) // return value
        var locExprs = loc1 + 1
        expressions.forEach {
            // todo: other expr types, ref params
            locExprs = it.code(locExprs)
        }
        val loc2 = locExprs
        codeArray.put(loc2, Call(symbolicAddress = routine.name))
        // store after call for each ref param
        return loc2 + 1
    }
}
