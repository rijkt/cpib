package ch.fhnw.parser.abstract

import ch.fhnw.tokens.groups.Type

interface IAbsNode {
    fun check(): Type
}
