package ch.fhnw.parser.abstract

import ch.fhnw.checker.TypeError
import ch.fhnw.checker.UndeclaredIdentifierError
import ch.fhnw.parser.GrammarError
import ch.fhnw.symboltable.DeclarationLocation
import ch.fhnw.tokens.groups.MechMode
import ch.fhnw.tokens.groups.Operator
import ch.fhnw.tokens.groups.Type
import ch.fhnw.vm.*
import ch.fhnw.vm.Code.Companion.codeArray

interface IAbsExpr : IAbsNode, IAbsCodeGeneration {
    fun lvalue(): Boolean
}

data class AbsDyadicExpr(private val opr: Operator, private val lAbsExpr: IAbsExpr, private val rAbsExpr: IAbsExpr) :
        IAbsExpr {
    override fun check(): Type {
        val leftType = lAbsExpr.check()
        val rightType = rAbsExpr.check()
        if (leftType != rightType) {
            throw TypeError("$leftType and $rightType do not match for operation $opr")
        }
        if (leftType == Type.VOID) {
            throw AssertionError("Did not expect type VOID")
        }
        // now both types are the same
        val ints = setOf(Type.INT32, Type.INT64, Type.INT1024)
        when (opr) {
            in setOf(Operator.CAND, Operator.COR) -> {
                if (leftType == Type.BOOL) {
                    return Type.BOOL
                } else {
                    throw GrammarError("Can't apply operator $opr to type $leftType")
                }
            }
            in setOf(
                    Operator.GE,
                    Operator.GT,
                    Operator.LE,
                    Operator.LT)
            -> {
                if (leftType in ints) {
                    return Type.BOOL
                } else {
                    throw GrammarError("Can't apply operator $opr to type $leftType")
                }
            }
            in setOf(
                    Operator.MINUS,
                    Operator.MOD_E,
                    Operator.MOD_F,
                    Operator.MOD_T,
                    Operator.PLUS,
                    Operator.TIMES,
                    Operator.DIV_F,
                    Operator.DIV_E,
                    Operator.DIV_T
            ) -> {
                if (leftType in ints) {
                    return leftType
                } else {
                    throw GrammarError("Can't apply operator $opr to type $leftType")
                }
            }
            in setOf(Operator.NE, Operator.EQ) -> return Type.BOOL
            else -> throw AssertionError("$opr is not a dyadic expression") // just for NOT
        }
    }

    override fun code(loc: Int): Int {
        val loc1 = lAbsExpr.code(loc)
        val loc2 = rAbsExpr.code(loc1)
        when (opr) {
            Operator.MINUS -> {
                codeArray.put(loc2, SubInt())
                return loc2 + 1
            }
            Operator.PLUS -> {
                codeArray.put(loc2, AddInt())
                return loc2 + 1
            }
            Operator.TIMES -> {
                codeArray.put(loc2, MultInt())
                return loc2 + 1
            }
            Operator.DIV_T -> {
                codeArray.put(loc2, DivTruncInt())
                return loc2 + 1
            }
            Operator.MOD_T -> {
                codeArray.put(loc2, ModTruncInt())
                return loc2 + 1
            }
            Operator.EQ -> {
                codeArray.put(loc2, EqInt())
                return loc2 + 1
            }
            Operator.GE -> {
                codeArray.put(loc2, GeInt())
                return loc2 + 1
            }
            Operator.GT -> {
                codeArray.put(loc2, GtInt())
                return loc2 + 1
            }
            Operator.LE -> {
                codeArray.put(loc2, LeInt())
                return loc2 + 1
            }
            Operator.LT -> {
                codeArray.put(loc2, LtInt())
                return loc2 + 1
            }
            Operator.CAND -> {
                codeArray.put(loc2, MultInt())
                return loc2 + 1
            }
            Operator.COR -> {
                codeArray.put(loc2, AddInt())
                return loc2 + 1
            }
            else -> throw ExecutionError("operator ${this.opr} not supported in vm")
        }
    }

    override fun lvalue(): Boolean = false
}

data class AbsMonadicExpr(private val opr: Operator, private val absExpr: IAbsExpr) : IAbsExpr {
    override fun check(): Type {
        val type = absExpr.check()
        if (type === Type.VOID) throw AssertionError("Did not expect void")
        when (opr) {
            Operator.MINUS -> {
                if (type in setOf(Type.INT32, Type.INT64, Type.INT1024)) {
                    return type
                } else {
                    throw GrammarError("Did not expect type $type for unary minus")
                }
            }
            Operator.NOT -> {
                if (type === Type.BOOL) {
                    return type
                } else {
                    throw GrammarError("Did not expect type $type for operator NOT")
                }
            }
            else -> throw AssertionError("$opr is not a monadic expression")
        }
    }

    override fun code(loc: Int): Int {
        val loc1 = absExpr.code(loc)
        when (opr) {
            Operator.MINUS -> {
                codeArray.put(loc1, NegInt())
                return loc1 + 1
            }
            Operator.NOT -> {
                codeArray.put(loc1, LoadImInt(0)) // TODO value correct?
                val loc2 = loc1 + 1
                codeArray.put(loc2, EqInt())
                return loc2 + 1
            }
            else -> throw ExecutionError("operator ${this.opr} not supported in vm")
        }
    }

    override fun lvalue(): Boolean = false
}

data class AbsBoolLiteralExpr(private val bool: Boolean) : IAbsExpr {
    override fun check(): Type = Type.BOOL

    override fun code(loc: Int): Int {
        var value = 0
        if (bool) {
            value = 1
        }
        codeArray.put(loc, LoadImInt(value))
        return loc + 1
    }

    override fun lvalue(): Boolean = false
}

data class AbsIntLiteralExpr(private val int: Int) : IAbsExpr {
    override fun check(): Type = Type.INT1024 // todo: check for size

    override fun code(loc: Int): Int {
        codeArray.put(loc, LoadImInt(int))
        return loc + 1
    }

    override fun lvalue(): Boolean = false
}

/**
 * Expression for a single variable
 */
data class AbsStoreExpr(private val ident: AbsIdentifier, private val isInitialization: Boolean) : IAbsExpr { // todo: check if needed
    var write = false
    override fun check(): Type {
        val variable = ident.environment.getVariable(ident.name)
        return variable?.type ?: throw UndeclaredIdentifierError("${ident.name} was not declared")
    }

    override fun code(loc: Int): Int {
        val variable = ident.environment.getVariable(ident.name)
                ?: throw AssertionError("Variable in AbsStoreExpr.code null")
        var loc2 = loc + 1
        when (variable.declarationLocation) {
            DeclarationLocation.GLOBAL -> {
                codeArray.put(loc, LoadImInt(variable.relAddress)) // in global block rel = absolute
            }
            DeclarationLocation.LET -> {
                codeArray.put(loc, LoadAddrLet(variable.relAddress))
            }
            else -> {
                codeArray.put(loc, LoadAddrRel(variable.relAddress))
                val isIndirectAccess = variable.mechMode == MechMode.REF
                if (isIndirectAccess) {
                    codeArray.put(loc2, Deref())
                    loc2 += 1
                }
            }
        }
        // up to here: l-value. If this is not a write, it's an r-value -> deref
        if (!this.write) {
            codeArray.put(loc2, Deref())
            loc2 += 1
        }
        return loc2
    }

    fun write(bool: Boolean) {
        write = bool
    }

    fun write(): Boolean {
        return write
    }

    override fun lvalue(): Boolean = true // todo: this isn't always true. merge with write logic?
}

data class AbsFunCallExpr(val routineCall: RoutineCall) : IAbsExpr {
    override fun check(): Type {
        return routineCall.check()
    }

    override fun code(loc: Int): Int {
        return routineCall.code(loc)
    }

    override fun lvalue(): Boolean = false
}
