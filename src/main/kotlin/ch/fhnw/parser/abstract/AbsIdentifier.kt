package ch.fhnw.parser.abstract

import ch.fhnw.symboltable.Environment

data class AbsIdentifier(val name: String, val environment: Environment)
