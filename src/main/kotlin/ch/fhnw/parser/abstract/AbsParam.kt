package ch.fhnw.parser.abstract

import ch.fhnw.tokens.groups.ChangeMode
import ch.fhnw.tokens.groups.FlowMode
import ch.fhnw.tokens.groups.MechMode
import ch.fhnw.tokens.groups.Type

interface IAbsParam: IAbsCodeGeneration

data class AbsParam(
    val flowMode: FlowMode?,
    val mechMode: MechMode?,
    val changeMode: ChangeMode?,
    val type: Type,
    val ident: String
) : IAbsParam {

    override fun code(loc: Int): Int {
        return loc
    }
}
