package ch.fhnw.parser.abstract

interface IAbsCodeGeneration {
    fun code(loc: Int): Int
}
