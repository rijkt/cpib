package ch.fhnw.parser.abstract

import ch.fhnw.tokens.groups.Type
import ch.fhnw.vm.Code.Companion.codeArray
import ch.fhnw.vm.Stop

interface IAbsProgram : IAbsNode, IAbsCodeGeneration

data class AbsProgram(val ident: AbsIdentifier, val progParams: List<IAbsProgParam>, val decls: List<IAbsDecl>, val cmds: List<IAbsCmd>) : IAbsProgram {
    override fun check(): Type {
        decls.forEach { it.check() } // check cmds in declarations
        cmds.forEach { it.check() }
        return Type.VOID
    }

    override fun code(loc: Int): Int {
        var loc1 = loc
        for (pp in progParams) {
            loc1 = pp.code(loc1)
        }
        for (d in decls) {
            if (d::class == AbsStoDecl::class) {
                loc1 = d.code(loc1)
            }
        }
        for (c in cmds) {
            loc1 = c.code(loc1)
        }
        codeArray.put(loc1, Stop())
        loc1++

        for (decl in decls) {
            when (decl::class) {
                AbsFunDecl::class -> {
                    val funLocation = loc1
                    loc1 = decl.code(loc1)
                    val funDecl = decl as AbsFunDecl
                    val funIdent = funDecl.ident
                    val routine = funIdent.environment.getRoutine(funIdent.name)
                    routine!!.address = funLocation
                }
                AbsProcDecl::class -> {
                    val procLocation = loc1
                    loc1 = decl.code(loc1)
                    val procDecl = decl as AbsProcDecl
                    val procIdent = procDecl.ident
                    val routine = procIdent.environment.getRoutine(procIdent.name)
                    routine!!.address = procLocation
                }
            }
        }
        return loc1
    }
}
