package ch.fhnw.parser.abstract

import ch.fhnw.tokens.groups.ChangeMode
import ch.fhnw.tokens.groups.FlowMode
import ch.fhnw.tokens.groups.Type

interface IAbsProgParam: IAbsCodeGeneration

data class AbsProgParam(val flowmode: FlowMode?, val changemode: ChangeMode?, val type: Type, val ident: String) : IAbsProgParam {
    override fun code(loc: Int): Int {
        return loc
    }
}
