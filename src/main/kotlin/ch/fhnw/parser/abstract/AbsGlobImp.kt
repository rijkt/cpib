package ch.fhnw.parser.abstract

import ch.fhnw.tokens.groups.ChangeMode
import ch.fhnw.tokens.groups.FlowMode

data class AbsGlobImp(val flowMode: FlowMode?, val changeMode: ChangeMode?, val ident: String): IAbsCodeGeneration {
    override fun code(loc: Int): Int {
        return loc
    }
}
