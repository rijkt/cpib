package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsStoDecl

interface IRepSemicolonStoDecl {
    fun toAbsSyn(): List<AbsStoDecl>
}

class RepSemicolonStoDecl(private val stoDecl: IStoDecl, private val repSemicolonStoDecl: IRepSemicolonStoDecl) :
    IRepSemicolonStoDecl {
    override fun toAbsSyn(): List<AbsStoDecl> {
        val decls = mutableListOf(stoDecl.toAbsSyn())
        decls.addAll(repSemicolonStoDecl.toAbsSyn())
        return decls
    }
}

class RepSemicolonStoDeclEps : IRepSemicolonStoDecl {
    override fun toAbsSyn(): List<AbsStoDecl> = emptyList()
}
