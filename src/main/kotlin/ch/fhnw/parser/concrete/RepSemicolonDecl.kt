package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsDecl

interface IRepSemicolonDecl {
    fun toAbsSyn(): Collection<IAbsDecl>
}

class RepSemicolonDecl(private val decl: IDecl, private val repSemicolonDecl: IRepSemicolonDecl) : IRepSemicolonDecl {
    override fun toAbsSyn(): Collection<IAbsDecl> {
        val decls = mutableListOf(decl.toAbsSyn())
        decls.addAll(repSemicolonDecl.toAbsSyn())
        return decls
    }
}

class RepSemicolonDeclEps : IRepSemicolonDecl {
    override fun toAbsSyn(): Collection<IAbsDecl> = emptyList()
}
