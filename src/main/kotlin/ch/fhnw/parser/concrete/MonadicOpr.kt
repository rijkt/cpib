package ch.fhnw.parser.concrete

import ch.fhnw.tokens.groups.Operator

interface IMonadicOpr {
    fun toAbsSyn(): Operator
}

class MonadicOprAdd(val opr: Operator) : IMonadicOpr {
    override fun toAbsSyn(): Operator = opr
}

class MonadicOprNot : IMonadicOpr {
    override fun toAbsSyn(): Operator = Operator.NOT
}
