package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsParam

interface IOptParamRepCommaParam {
    fun toAbsSyn(): List<AbsParam>?
}

class OptParamRepCommaParam(private val param: IParam, private val repCommaParam: IRepCommaParam) : IOptParamRepCommaParam {
    override fun toAbsSyn(): List<AbsParam> {
        val params = mutableListOf(param.toAbsSyn())
        params.addAll(repCommaParam.toAbsSyn())
        return params
    }
}

class OptParamRepCommaParamEps: IOptParamRepCommaParam {
    override fun toAbsSyn(): List<AbsParam>? = null
}