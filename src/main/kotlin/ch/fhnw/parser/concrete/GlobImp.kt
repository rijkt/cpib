package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsGlobImp

interface IGlobImp {
    fun toAbsSyn(): AbsGlobImp
}

class GlobImp(private val optFlowmode: IOptFlowmode, private val optChangemode: IOptChangemode, val ident: String) :
    IGlobImp {
    override fun toAbsSyn(): AbsGlobImp {
        return AbsGlobImp(optFlowmode.toAbsSyn(), optChangemode.toAbsSyn(), ident)
    }
}
