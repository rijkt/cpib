package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsProgParam

interface IOptProgParamRepCommaProgParam {
    fun toAbsSyn(): List<AbsProgParam>
}

class OptProgParamRepCommaProgParam(
    private val progParam: IProgParam,
    private val repCommaProgParam: IRepCommaProgParam
) : IOptProgParamRepCommaProgParam {
    override fun toAbsSyn(): List<AbsProgParam> {
        val params = mutableListOf(progParam.toAbsSyn()) // at least one param
        params.addAll(repCommaProgParam.toAbsSyn()) // could be empty
        return params
    }
}

class OptProgParamRepCommaProgParamEps : IOptProgParamRepCommaProgParam {
    override fun toAbsSyn(): List<AbsProgParam> = emptyList()
}
