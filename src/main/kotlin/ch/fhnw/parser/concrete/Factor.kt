package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsBoolLiteralExpr
import ch.fhnw.parser.abstract.AbsIntLiteralExpr
import ch.fhnw.parser.abstract.AbsMonadicExpr
import ch.fhnw.parser.abstract.IAbsExpr

interface IFactor {
    fun toAbsSyn(): IAbsExpr
}

class IntLiteral(private val literal: Int): IFactor {
    override fun toAbsSyn(): IAbsExpr = AbsIntLiteralExpr(literal)
}

class BooleanLiteral(private val literal: Boolean): IFactor {
    override fun toAbsSyn(): IAbsExpr = AbsBoolLiteralExpr(literal)
}

class FactorIdent(private val ident: Identifier, private val optInitOrExprList: IOptInitOrExprList): IFactor {
    override fun toAbsSyn(): IAbsExpr  = optInitOrExprList.toAbsSyn(ident)
}

class FactorMonOpr(private val operator: IMonadicOpr, private val factor: IFactor): IFactor {
    override fun toAbsSyn(): IAbsExpr = AbsMonadicExpr(operator.toAbsSyn(), factor.toAbsSyn())
}

class FactorLparen(private val expr: IExpr) : IFactor {
    override fun toAbsSyn(): IAbsExpr = expr.toAbsSyn()
}
