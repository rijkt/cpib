package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsExpr

interface IExprList {
    fun toAbsSyn(): List<IAbsExpr>
}

class ExprList(val optExprRepCommaExpr: IOptExprRepCommaExpr) : IExprList {
    override fun toAbsSyn(): List<IAbsExpr> = optExprRepCommaExpr.toAbsSyn()
}