package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsStoDecl

interface IOptLocalCpsStoDecl {
    fun toAbsSyn(): List<AbsStoDecl>?
}

class OptLocalCpsStoDecl(private val cpsStoDecl: ICpsStoDecl) : IOptLocalCpsStoDecl {
    override fun toAbsSyn(): List<AbsStoDecl> {
        return cpsStoDecl.toAbsSyn()
    }
}

class OptLocalCpsStoDeclEps: IOptLocalCpsStoDecl {
    override fun toAbsSyn(): Nothing? = null
}