package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsCmd

interface IRepSemicolonCmd {
    fun toAbsSyn(): List<IAbsCmd>
}

class RepSemicolonCmd(val cmd: ICmd, val repSemicolonCmd: IRepSemicolonCmd) : IRepSemicolonCmd {
    override fun toAbsSyn(): List<IAbsCmd> {
        val cpsCmd = mutableListOf(cmd.toAbsSyn())
        cpsCmd.addAll(repSemicolonCmd.toAbsSyn())
        return cpsCmd
    }
}

class RepSemicolonCmdEps : IRepSemicolonCmd {
    override fun toAbsSyn(): List<IAbsCmd> = emptyList()
}
