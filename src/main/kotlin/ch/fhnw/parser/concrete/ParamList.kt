package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsParam

interface IParamList {
    fun toAbsSyn(): List<AbsParam>?
}

class ParamList(private val optParamRepCommaParam: IOptParamRepCommaParam) : IParamList {
    override fun toAbsSyn(): List<AbsParam>? = optParamRepCommaParam.toAbsSyn()
}