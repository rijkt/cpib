package ch.fhnw.parser.concrete

import ch.fhnw.tokens.groups.FlowMode

interface IOptFlowmode {
    fun toAbsSyn(): FlowMode?
}

class FlowMode(private val flowMode: FlowMode) : IOptFlowmode {
    override fun toAbsSyn(): FlowMode? = this.flowMode
}

class OptFlowModeEps : IOptFlowmode {
    override fun toAbsSyn(): FlowMode? = null
}
