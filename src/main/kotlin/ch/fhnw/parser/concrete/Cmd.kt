package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.*

interface ICmd {
    fun toAbsSyn(): IAbsCmd
}

class CmdSkip : ICmd {
    override fun toAbsSyn(): IAbsCmd = AbsSkipCmd()
}

class CmdAssignment(val expr1: IExpr, val expr2: IExpr) : ICmd {
    override fun toAbsSyn(): IAbsCmd = AbsAssiCmd(expr1.toAbsSyn(), expr2.toAbsSyn())
}

class CmdIf(val expr: IExpr, val cpsCmd: ICpsCmd, val optElseCmd: IOptElseCpsCmd) : ICmd {
    override fun toAbsSyn(): IAbsCmd = AbsCondCmd(expr.toAbsSyn(), cpsCmd.toAbsSyn(), optElseCmd.toAbsSyn())
}

class CmdLet(val cpsStoDecl: ICpsStoDecl, val cpsCmd: ICpsCmd) : ICmd {
    override fun toAbsSyn(): IAbsCmd = AbsLetCmd(cpsStoDecl.toAbsSyn(), cpsCmd.toAbsSyn())
}

class CmdWhile(val expr: IExpr, val cpsCmd: ICpsCmd) : ICmd {
    override fun toAbsSyn(): IAbsCmd = AbsWhileCmd(expr.toAbsSyn(), cpsCmd.toAbsSyn())
}

class CmdCall(val ident: Identifier, val exprList: IExprList, val optGlobInits: IOptGlobInits) : ICmd {
    override fun toAbsSyn(): IAbsCmd = AbsProcCmd(RoutineCall(ident.toAbsyn(), exprList.toAbsSyn()), optGlobInits.toAbsSyn())
}

class CmdDebugIn(val expr: IExpr) : ICmd {
    override fun toAbsSyn(): IAbsCmd = AbsInputCmd(expr.toAbsSyn())
}

class CmdDebugOut(val expr: IExpr) : ICmd {
    override fun toAbsSyn(): IAbsCmd = AbsOutputCmd(expr.toAbsSyn())
}
