package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsCmd

interface ICpsCmd {
    fun toAbsSyn(): List<IAbsCmd>
}

class CpsCmd(val cmd: ICmd, val repSemicolonCmd: IRepSemicolonCmd) : ICpsCmd {
    override fun toAbsSyn(): List<IAbsCmd> {
        val cpsCmd = mutableListOf(cmd.toAbsSyn())
        cpsCmd.addAll(repSemicolonCmd.toAbsSyn())
        return cpsCmd
    }
}