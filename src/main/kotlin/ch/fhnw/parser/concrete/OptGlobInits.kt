package ch.fhnw.parser.concrete

interface IOptGlobInits {
    fun toAbsSyn(): List<String>
}

class OptGlobInits(private val ident: String, private val repCommaIdent: IRepCommaIdent) : IOptGlobInits {
    override fun toAbsSyn(): List<String> {
        val params = mutableListOf(ident)
        params.addAll(repCommaIdent.toAbsSyn())
        return params
    }
}

class OptGlobInitsEps : IOptGlobInits {
    override fun toAbsSyn(): List<String> = emptyList()
}
