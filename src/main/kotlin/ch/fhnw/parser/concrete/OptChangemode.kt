package ch.fhnw.parser.concrete

import ch.fhnw.tokens.groups.ChangeMode

interface IOptChangemode {
    fun toAbsSyn(): ChangeMode?
}

class OptChangemode(private val mode: ChangeMode) : IOptChangemode {
    override fun toAbsSyn(): ChangeMode? = mode
}

class OptChangeModeEps : IOptChangemode {
    override fun toAbsSyn(): ChangeMode? = null
}
