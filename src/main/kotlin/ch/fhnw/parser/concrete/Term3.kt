package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsExpr

interface ITerm3 {
    fun toAbsSyn(): IAbsExpr
}

class Term3(val factor: IFactor, val repMultOprFactor: IRepMultOprFactor) : ITerm3 {
    override fun toAbsSyn(): IAbsExpr = repMultOprFactor.toAbsSyn(factor)
}