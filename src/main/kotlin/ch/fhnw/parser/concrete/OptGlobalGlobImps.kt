package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsGlobImp

interface IOptGlobalGlobImps {
    fun toAbsSyn(): List<AbsGlobImp>?
}

class OptGlobalGlobImps(private val globImps: IGlobImps) : IOptGlobalGlobImps {
    override fun toAbsSyn(): List<AbsGlobImp> = globImps.toAbsSyn()
}

class OptGlobalGlobImpsEps: IOptGlobalGlobImps {
    override fun toAbsSyn(): Nothing? = null
}