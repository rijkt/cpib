package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsDecl

interface ICpsDecl {
    fun toAbsSyn(): List<IAbsDecl>
}

class CpsDecl(private val decl: IDecl, private val repSemicolonDecl: IRepSemicolonDecl) : ICpsDecl {
    override fun toAbsSyn(): List<IAbsDecl> {
        val absDecls = mutableListOf(decl.toAbsSyn())
        absDecls.addAll(repSemicolonDecl.toAbsSyn())
        return absDecls
    }
}
