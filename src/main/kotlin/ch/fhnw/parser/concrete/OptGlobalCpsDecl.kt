package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsDecl

interface IOptGlobalCpsDecl {
    fun toAbsSyn(): List<IAbsDecl>
}

class OptGlobalCpsDecl(private val cpsDecl: ICpsDecl) : IOptGlobalCpsDecl {
    override fun toAbsSyn(): List<IAbsDecl> = cpsDecl.toAbsSyn()
}

class OptGlobalCpsDeclEps: IOptGlobalCpsDecl {
    override fun toAbsSyn(): List<IAbsDecl> = emptyList()
}
