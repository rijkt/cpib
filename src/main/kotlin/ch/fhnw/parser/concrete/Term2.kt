package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsExpr

interface ITerm2 {
    fun toAbsSyn(): IAbsExpr
}

class Term2(val term3: ITerm3, val repAddOprTerm3: IRepAddOprTerm3) : ITerm2 {
    override fun toAbsSyn(): IAbsExpr = repAddOprTerm3.toAbsSyn(term3.toAbsSyn())
}