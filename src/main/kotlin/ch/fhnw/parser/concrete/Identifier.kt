package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsIdentifier
import ch.fhnw.symboltable.Environment

class Identifier(val name: String, val environment: Environment) {
    fun toAbsyn() = AbsIdentifier(name, environment)
}
