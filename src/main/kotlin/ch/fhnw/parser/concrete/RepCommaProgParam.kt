package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsProgParam

interface IRepCommaProgParam {
    fun toAbsSyn(): Collection<AbsProgParam>
}

class RepCommaProgParam(private val progParam: IProgParam, private val repCommaProgParam: IRepCommaProgParam) :
    IRepCommaProgParam {
    override fun toAbsSyn(): Collection<AbsProgParam> {
        val params = mutableListOf(progParam.toAbsSyn())
        params.addAll(repCommaProgParam.toAbsSyn())
        return params
    }
}

class RepCommaProgParamEps : IRepCommaProgParam {
    override fun toAbsSyn(): Collection<AbsProgParam> = emptyList()
}
