package ch.fhnw.parser.concrete

interface IRepCommaIdent {
    fun toAbsSyn(): List<String>
}

class RepCommaIdent(private val ident: String, private val repCommaIdent: IRepCommaIdent) : IRepCommaIdent {
    override fun toAbsSyn(): List<String> {
        val params = mutableListOf(ident)
        params.addAll(repCommaIdent.toAbsSyn())
        return params
    }
}

class RepCommaIdentEps : IRepCommaIdent {
    override fun toAbsSyn(): List<String> = emptyList()
}
