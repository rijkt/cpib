package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsDyadicExpr
import ch.fhnw.parser.abstract.IAbsExpr
import ch.fhnw.tokens.groups.Operator

interface IOptRelOprTerm2 {
    fun toAbsSyn(term2: ITerm2): IAbsExpr
}

class OptRelOprTerm2RelOpr(val operator: Operator, val term2: ITerm2) : IOptRelOprTerm2 {
    override fun toAbsSyn(term2: ITerm2): IAbsExpr = AbsDyadicExpr(operator, term2.toAbsSyn(), this.term2.toAbsSyn())
}

class OptRelOprTerm2Eps : IOptRelOprTerm2 {
    override fun toAbsSyn(term2: ITerm2): IAbsExpr = term2.toAbsSyn()
}
