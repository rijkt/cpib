package ch.fhnw.parser.concrete

interface IExpr {
    fun toAbsSyn(): ch.fhnw.parser.abstract.IAbsExpr
}

class Expr(val term1: ITerm1, val repBoolOprTerm1: IRepBoolOprTerm1) : IExpr {
    override fun toAbsSyn(): ch.fhnw.parser.abstract.IAbsExpr = repBoolOprTerm1.toAbsSyn(term1)
}