package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsFunDecl

interface IFunDecl {
    fun toAbsSyn(): AbsFunDecl
}

class FunDecl(
    private val ident: Identifier,
    private val paramList: IParamList,
    // implicit: returns
    private val returns: IStoDecl,
    private val optGlobalGlobImps: IOptGlobalGlobImps,
    private val optLocalCpsStoDecl: IOptLocalCpsStoDecl,
    private val cpsCmd: ICpsCmd
) : IFunDecl {
    override fun toAbsSyn(): AbsFunDecl {
        return AbsFunDecl(
            ident.toAbsyn(),
            paramList.toAbsSyn(),
            returns.toAbsSyn(),
            optGlobalGlobImps.toAbsSyn(),
            optLocalCpsStoDecl.toAbsSyn(),
            cpsCmd.toAbsSyn()
        )
    }
}
