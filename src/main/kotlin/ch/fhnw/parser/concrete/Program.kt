package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsProgram

interface IProgram {
    fun toAbsSyn(): AbsProgram
}

class Program(
        private val ident: Identifier,
        private val progParamList: IProgParamList,
        private val optGlobalCpsDecl: IOptGlobalCpsDecl,
        private val cpsCmd: ICpsCmd
) : IProgram {
    override fun toAbsSyn(): AbsProgram {
        return AbsProgram(ident.toAbsyn(), progParamList.toAbsSyn(), optGlobalCpsDecl.toAbsSyn(), cpsCmd.toAbsSyn())
    }
}
