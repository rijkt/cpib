package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsStoDecl

interface ICpsStoDecl {
    fun toAbsSyn(): List<AbsStoDecl>
}

class CpsStoDecl(private val stoDecl: IStoDecl, private val repSemicolonStoDecl: IRepSemicolonStoDecl) : ICpsStoDecl {
    override fun toAbsSyn(): List<AbsStoDecl> {
        val stoDecls = mutableListOf(stoDecl.toAbsSyn())
        stoDecls.addAll(repSemicolonStoDecl.toAbsSyn())
        return stoDecls
    }
}
