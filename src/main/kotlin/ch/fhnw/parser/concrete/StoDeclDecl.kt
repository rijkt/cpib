package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsDecl

interface IDecl {
    fun toAbsSyn(): IAbsDecl
}

class StoDeclDecl(private val stoDecl: IStoDecl) : IDecl {
    override fun toAbsSyn(): IAbsDecl = stoDecl.toAbsSyn()
}

class FunDeclDecl(private val funDecl: IFunDecl) : IDecl {
    override fun toAbsSyn(): IAbsDecl = funDecl.toAbsSyn()
}

class ProcDeclDecl(private val procDecl: IProcDecl) : IDecl {
    override fun toAbsSyn(): IAbsDecl = procDecl.toAbsSyn()
}
