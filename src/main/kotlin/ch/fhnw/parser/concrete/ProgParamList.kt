package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsProgParam

interface IProgParamList {
    fun toAbsSyn(): List<IAbsProgParam>
}

class ProgParamList(private val optProgParamRepCommaProgParam: IOptProgParamRepCommaProgParam) : IProgParamList {
    override fun toAbsSyn(): List<IAbsProgParam> = this.optProgParamRepCommaProgParam.toAbsSyn()
}
