package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsParam

interface IRepCommaParam {
    fun toAbsSyn(): List<AbsParam>
}

class RepCommaParam(private val param: IParam, private val repCommaParam: IRepCommaParam) : IRepCommaParam {
    override fun toAbsSyn(): List<AbsParam> {
        val params = mutableListOf(param.toAbsSyn())
        params.addAll(repCommaParam.toAbsSyn())
        return params
    }
}

class RepCommaParamEps : IRepCommaParam {
    override fun toAbsSyn(): List<AbsParam> = emptyList()
}
