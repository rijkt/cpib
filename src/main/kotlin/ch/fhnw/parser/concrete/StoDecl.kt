package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsStoDecl

interface IStoDecl {
    fun toAbsSyn(): AbsStoDecl
}

class StoDecl(val optChangemode: IOptChangemode, val typedIdent: TypedIdent) : IStoDecl {
    override fun toAbsSyn(): AbsStoDecl {
        val absTypedIdent = typedIdent.toAbsSyn()
        return AbsStoDecl(optChangemode.toAbsSyn(), absTypedIdent.type, absTypedIdent.ident)
    }
}
