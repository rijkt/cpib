package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsFunCallExpr
import ch.fhnw.parser.abstract.AbsStoreExpr
import ch.fhnw.parser.abstract.IAbsExpr
import ch.fhnw.parser.abstract.RoutineCall

/**
 * comes after an IDENT, forms IDENT [INIT | exprList], a factor
 */
interface IOptInitOrExprList {
    fun toAbsSyn(ident: Identifier): IAbsExpr
}

class OptInitOrExprListInit : IOptInitOrExprList {
    override fun toAbsSyn(ident: Identifier): IAbsExpr {
        return AbsStoreExpr(ident.toAbsyn(), true)
    }
}

class OptInitOrExprListExprList(private val exprList: IExprList) : IOptInitOrExprList {
    override fun toAbsSyn(ident: Identifier): IAbsExpr {
        return AbsFunCallExpr(RoutineCall(ident.toAbsyn(), exprList.toAbsSyn()))
    }
}

class OptInitOrExprListEps : IOptInitOrExprList {
    override fun toAbsSyn(ident: Identifier): IAbsExpr {
        return AbsStoreExpr(ident.toAbsyn(), false)
    }
}
