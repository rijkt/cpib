package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsDyadicExpr
import ch.fhnw.parser.abstract.IAbsExpr
import ch.fhnw.tokens.groups.Operator

interface IRepBoolOprTerm1 {
    fun toAbsSyn(term1: ITerm1): IAbsExpr
}

class RepBoolOprTerm1BoolOpr(val operator: Operator, val term1: ITerm1, val repBoolOprTerm1: IRepBoolOprTerm1) : IRepBoolOprTerm1 {
    override fun toAbsSyn(term1: ITerm1): IAbsExpr = AbsDyadicExpr(operator, term1.toAbsSyn(), repBoolOprTerm1.toAbsSyn(this.term1))
}

class RepBoolOprTerm1Eps(): IRepBoolOprTerm1 {
    override fun toAbsSyn(term1: ITerm1): IAbsExpr = term1.toAbsSyn()
}
