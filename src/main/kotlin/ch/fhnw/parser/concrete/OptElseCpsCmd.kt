package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsCmd

interface IOptElseCpsCmd {
    fun toAbsSyn(): List<IAbsCmd>
}

class OptElseCpsCmd(val cpsCmd: ICpsCmd) : IOptElseCpsCmd {
    override fun toAbsSyn(): List<IAbsCmd> = cpsCmd.toAbsSyn()
}

class OptElseCpsCmdEps : IOptElseCpsCmd {
    override fun toAbsSyn(): List<IAbsCmd> = emptyList()
}
