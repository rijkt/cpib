package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsDyadicExpr
import ch.fhnw.parser.abstract.IAbsExpr
import ch.fhnw.tokens.groups.Operator

interface IRepMultOprFactor {
    fun toAbsSyn(factor: IFactor): IAbsExpr
}

class RepMultOprFactorEps: IRepMultOprFactor {
    override fun toAbsSyn(factor: IFactor): IAbsExpr = factor.toAbsSyn()
}

class RepMultOprFactorOpr(val opr: Operator, val factor: IFactor, val repMULTOPRfactor: IRepMultOprFactor) : IRepMultOprFactor {
    override fun toAbsSyn(factor: IFactor): IAbsExpr = AbsDyadicExpr(opr, factor.toAbsSyn(), repMULTOPRfactor.toAbsSyn(this.factor))
}