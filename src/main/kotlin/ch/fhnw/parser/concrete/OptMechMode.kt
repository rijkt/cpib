package ch.fhnw.parser.concrete

import ch.fhnw.tokens.groups.MechMode

interface IOptMechmode {
    fun toAbsSyn(): MechMode?
}

class OptMechmode(private val mechMode: MechMode) : IOptMechmode {
    override fun toAbsSyn() = mechMode
}

class OptMechmodeEps: IOptMechmode {
    override fun toAbsSyn(): Nothing? = null
}