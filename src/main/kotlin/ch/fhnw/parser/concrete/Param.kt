package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsParam

interface IParam {
    fun toAbsSyn(): AbsParam
}

class Param(
    private val optFlowmode: IOptFlowmode,
    private val optMechmode: IOptMechmode,
    private val optChangemode: IOptChangemode,
    private val typedIdent: TypedIdent
) : IParam {
    override fun toAbsSyn(): AbsParam {
        val absTypedIdent = typedIdent.toAbsSyn()
        return AbsParam(
            optFlowmode.toAbsSyn(),
            optMechmode.toAbsSyn(),
            optChangemode.toAbsSyn(),
            absTypedIdent.type,
            absTypedIdent.ident
        )
    }
}