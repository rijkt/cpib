package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsProgParam

interface IProgParam {
    fun toAbsSyn(): AbsProgParam
}

class ProgParam(
    private val optFlowmode: IOptFlowmode,
    private val optChangemode: IOptChangemode,
    private val typedIdent: ITypedIdent
) : IProgParam {
    override fun toAbsSyn(): AbsProgParam {
        val typedIdent = typedIdent.toAbsSyn()
        return AbsProgParam(optFlowmode.toAbsSyn(), optChangemode.toAbsSyn(), typedIdent.type, typedIdent.ident)
    }
}
