package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsProcDecl

interface IProcDecl {
    fun toAbsSyn(): AbsProcDecl
}

class ProcDecl(
    private val ident: Identifier,
    private val paramList: IParamList,
    private val optGlobalGlobImps: IOptGlobalGlobImps,
    private val optLocalCpsStoDecl: IOptLocalCpsStoDecl,
    private val cpsCmd: ICpsCmd
) : IProcDecl {
    override fun toAbsSyn(): AbsProcDecl {
        return AbsProcDecl(
            ident.toAbsyn(),
            paramList.toAbsSyn(),
            optGlobalGlobImps.toAbsSyn(),
            optLocalCpsStoDecl.toAbsSyn(),
            cpsCmd.toAbsSyn()
        )
    }
}
