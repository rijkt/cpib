package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsExpr

interface IOptExprRepCommaExpr {
    fun toAbsSyn(): List<IAbsExpr>
}

class OptExprRepCommaExpr(val expr: IExpr, val repCommaExpr: IRepCommaExpr) : IOptExprRepCommaExpr {
    override fun toAbsSyn(): List<IAbsExpr> {
        val exprList = mutableListOf(expr.toAbsSyn())
        exprList.addAll(repCommaExpr.toAbsSyn())
        return exprList
    }
}

class OptExprRepCommaExprEps: IOptExprRepCommaExpr {
    override fun toAbsSyn(): List<IAbsExpr> = emptyList()
}