package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsGlobImp

interface IGlobImps {
    fun toAbsSyn(): List<AbsGlobImp>
}

class GlobImps(private val globImp: IGlobImp, private val repCommaGlobImp: IRepCommaGlobImp) : IGlobImps {
    override fun toAbsSyn(): List<AbsGlobImp> {
        val globImps = mutableListOf(globImp.toAbsSyn())
        globImps.addAll(repCommaGlobImp.toAbsSyn())
        return globImps
    }
}