package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsDyadicExpr
import ch.fhnw.parser.abstract.IAbsExpr
import ch.fhnw.tokens.groups.Operator

interface IRepAddOprTerm3 {
    fun toAbsSyn(absDyadicExpr: IAbsExpr): IAbsExpr
}

class RepAddOprTerm3Eps: IRepAddOprTerm3 {
    override fun toAbsSyn(absDyadicExpr: IAbsExpr): IAbsExpr = absDyadicExpr
}

class RepAddOprTerm3Opr(val opr: Operator, val term3: ITerm3, val repAddOprTerm3: IRepAddOprTerm3):
    IRepAddOprTerm3 {
    override fun toAbsSyn(absDyadicExpr: IAbsExpr): IAbsExpr = repAddOprTerm3.toAbsSyn(AbsDyadicExpr(opr, absDyadicExpr, term3.toAbsSyn()))
}
