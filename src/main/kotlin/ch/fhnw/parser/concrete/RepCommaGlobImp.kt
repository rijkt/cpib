package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsGlobImp

interface IRepCommaGlobImp {
    fun toAbsSyn(): Collection<AbsGlobImp>
}

class RepCommaGlobImp(private val globImps: IGlobImps) : IRepCommaGlobImp {
    override fun toAbsSyn() = globImps.toAbsSyn()
}

class RepCommaGLobImpEps: IRepCommaGlobImp {
    override fun toAbsSyn(): List<AbsGlobImp> = emptyList()
}