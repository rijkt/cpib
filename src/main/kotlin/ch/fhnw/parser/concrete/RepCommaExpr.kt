package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsExpr

interface IRepCommaExpr {
    fun toAbsSyn(): List<IAbsExpr>
}

class RepCommaExpr(val expr: IExpr, val repCommaExpr: IRepCommaExpr) : IRepCommaExpr {
    override fun toAbsSyn(): List<IAbsExpr> {
        val exprList = mutableListOf(expr.toAbsSyn())
        exprList.addAll(repCommaExpr.toAbsSyn())
        return exprList
    }
}

class RepCommaExprEps: IRepCommaExpr {
    override fun toAbsSyn(): List<IAbsExpr> = emptyList()
}
