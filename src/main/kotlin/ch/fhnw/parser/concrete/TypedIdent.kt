package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.AbsTypedIdent
import ch.fhnw.tokens.groups.Type

interface ITypedIdent {
    fun toAbsSyn(): AbsTypedIdent
}

class TypedIdent(val ident: String, val type: Type) : ITypedIdent {
    override fun toAbsSyn(): AbsTypedIdent {
        return AbsTypedIdent(ident, type)
    }
}
