package ch.fhnw.parser.concrete

import ch.fhnw.parser.abstract.IAbsExpr

interface ITerm1 {
    fun toAbsSyn(): IAbsExpr
}

class Term1(val term2: ITerm2, val optRelOprTerm2: IOptRelOprTerm2): ITerm1 {
    override fun toAbsSyn(): IAbsExpr = optRelOprTerm2.toAbsSyn(term2)
}