package ch.fhnw

enum class Terminal {
    ADDOPR,
    BECOMES,
    BOOLOPR,
    CALL,
    CHANGEMODE,
    CONST,
    COLON,
    COMMA,
    DEBUGIN,
    DEBUGOUT,
    DIVOPR,
    DO,
    ELSE,
    ENDFUN,
    ENDIF,
    ENDLET,
    ENDPROC,
    ENDPROGRAM,
    ENDWHILE,
    EXCLAMATIONMARK,
    FUN,
    FLOWMODE,
    GLOBAL,
    IDENT,
    IF,
    INIT,
    LET,
    LITERAL,
    LOCAL,
    LPAREN,
    MECHMODE,
    MULTOPR,
    NOTOPR,
    PROC,
    PROGRAM,
    RELOPR,
    RETURNS,
    RPAREN,
    SKIP,
    SEMICOLON,
    SENTINEL,
    THEN,
    TYPE,
    WHILE
}