package ch.fhnw.generator

import ch.fhnw.parser.abstract.AbsProgram

class CodeGenerator {

    fun code(abstractTree: AbsProgram, location: Int) {
        abstractTree.code(location)
    }
}
