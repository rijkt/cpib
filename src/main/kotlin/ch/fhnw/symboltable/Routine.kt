package ch.fhnw.symboltable

import ch.fhnw.tokens.groups.Type

data class Routine(val name: String, val params: Map<String, Variable>, val returnType: Type, var address: Int? = null)
