package ch.fhnw.symboltable

import ch.fhnw.tokens.groups.ChangeMode
import ch.fhnw.tokens.groups.FlowMode
import ch.fhnw.tokens.groups.MechMode
import ch.fhnw.tokens.groups.Type

data class Variable(val ident: String, val flowMode: FlowMode?, val mechMode: MechMode?, val changeMode: ChangeMode?, val type: Type, val relAddress: Int, val declarationLocation: DeclarationLocation)
