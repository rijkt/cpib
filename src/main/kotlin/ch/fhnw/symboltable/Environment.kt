package ch.fhnw.symboltable

data class Environment(
    val vars: MutableMap<String, Variable>,
    private val routines: MutableMap<String, Routine>,
    private val parent: Environment?,
    val startAddress: Int
) {

    /**
     * Add variable to symbol table if it wasn't already declared
     *
     * @throws DuplicateDeclarationError
     */
    fun putVariable(identifier: String, variable: Variable) {
        if (vars.containsKey(identifier))
            throw DuplicateDeclarationError("Variable $identifier was already defined")
        else
            vars[identifier] = variable
    }

    fun getVariable(key: String): Variable? {
        val variable = vars[key]
        return if (variable == null && parent != null)
            parent.getVariable(key)
        else
            variable
    }

    /**
     * Add routine (fun & proc) to symbol table if it wasn't already declared
     *
     * @throws DuplicateDeclarationError
     */
    fun putRoutine(identifier: String, routine: Routine) {
        if (routines.containsKey(identifier))
            throw DuplicateDeclarationError("Routine $identifier was already defined")
        else
            routines[identifier] = routine
    }

    fun getRoutine(key: String): Routine? {
        val routine = routines[key]
        return if (routine == null && parent != null)
            parent.getRoutine(key)
        else
            routine
    }

    fun getParent(): Environment? {
        return parent
    }
}
