package ch.fhnw.symboltable

enum class DeclarationLocation{
    PARAM,
    LOCAL,
    RETURN,
    LET,
    GLOBAL
}
