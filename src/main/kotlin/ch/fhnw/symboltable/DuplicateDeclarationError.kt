package ch.fhnw.symboltable

class DuplicateDeclarationError(message: String?) : Error(message)
