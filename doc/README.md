Bibliographie
-------------
`latex bericht`

`bibtex bericht.aux`

`latex bericht`

`latex bericht`

`pdflatex bericht`

Word/Char count
---------------
`texcount [-char] bericht.tex`

Spell check
-----------
`aspell -c -t bericht.tex -d de_CH`
