\documentclass{fhnwreport} %
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{amsmath}
\usetikzlibrary{arrows}
\usepackage{helvet}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{rotating}
\renewcommand{\familydefault}{\sfdefault}
\renewcommand{\lstlistingname}{Code}
\renewcommand{\lstlistlistingname}{Codeverzeichnis}

%% add numbering to list of tables and listings
\renewcommand{\listoffigures}{\begingroup
  \tocsection
  \tocfile{\listfigurename}{lof}
  \endgroup}
\renewcommand{\listoftables}{\begingroup
  \tocsection
  \tocfile{\listtablename}{lot}
\endgroup}
\renewcommand{\lstlistoflistings}{\begingroup
  \tocsection
  \tocfile{\lstlistlistingname}{lol}
\endgroup}

%% add footnote for href
\newcommand\fnurl[2]{%
  \href{#1}{#2}\footnote{\url{#1}}%
}

%% configure listing package
\lstset{
  basicstyle=\ttfamily\small,
  columns=fullflexible,
  frame=single,
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space},
  numbers=left,
  stepnumber=5,
  numberstyle=\tiny\color{gray}
}

\lstdefinelanguage{Directorytree}{
  keepspaces=true
}

\emergencystretch 5em


\title{Blocks with local declarations for IML \\ Schlussbericht \\ Compilerbau, HS 2020, Team EG}
\author{Jürg Gnos, Pascal Ellenberger}

%! Suppress = LineBreak
\begin{document}
\maketitle

%\section*{Abstract}

\section{Idee der Erweiterung mit konkreten Beispielen}
Die Idee der Erweiterung ``Blocks with local declarations'' ist das Definieren von einem Konstrukt, welches es ermöglicht, dass Variablen innerhalb einer Methode nur in bestimmten Blöcken zugreifbar sind.\newline
Im untenstehenden Codeabschnitt (\ref{lst:example}) wird die Idee an einem Beispiel skizziert:

\lstinputlisting[label={lst:example}, caption={Beispielcode ``Blocks with local declarations''}]{code/example.iml}

In Zeile 4 signalisiert das Keyword \emph{let} den Start des Blocks mit lokalen Deklarationen. Zwischen dem \emph{let} in Zeile 4 und dem \emph{do} in Zeile 7 werden die Konstanten \emph{b} und \emph{c} als Integer deklariert. Zwischen dem \emph{do} in Zeile 7 und dem \emph{endlet} in Zeile 11 können die Konstanten \emph{b} und \emph{c} verwendet werden. Im Beispiel werden die Konstanten \emph{b} in Zeile 8 mit dem Wert 10 und \emph{c} in Zeile 9 mit dem Wert von \emph{a} initialisiert. Bis zum Keyword \emph{endlet} in Zeile 11 sind die beiden Konstanten \emph{b} und \emph{c} verfügbar, ab Zeile 12 nicht mehr.

\section{Lexikalische Syntax}
Die lexikalische Syntax wird um die Reserved Identifiers \emph{let} und \emph{endlet} erweitert, wie im Listing unten (\ref{lst:reserved}) verdeutlicht.
\begin{lstlisting}[label={lst:reserved}, caption={Erweiterung der Reserved Identifiers}]
	<reservedid> ::= let
	<reservedid> ::= endlet
\end{lstlisting}

Im Scanner werden die neuen Tokens \emph{LetToken} und \emph{EndLetToken} hinzugef"ugt.

\section{Grammatikalische Syntax}
Die Erweiterung von ``Blocks with local declarations'' macht eine Anpassung der grammatikalischen Syntax notwendig. Das Konstrukt wird in \emph{cmd} eingebaut. Untenstehendes Listing (\ref{lst:grammar}) zeigt die Anpassung der Grammatik:
\begin{lstlisting}[label={lst:grammar}, caption={Erweiterung der Grammatik}]
	<cmd> ::= let <cpsStoDecl> do <cpsCmd> endlet
\end{lstlisting}

\section{Kontext- und Typeinschränkungen}
\label{sec:restrictions}
Für die Erweiterung sind keine speziellen Scope- oder Typeinschränkungen notwendig. Im Deklarations- und dem Command-Block sollen die gleichen Regeln wie in den Pendants in Routinen und dem Programm gelten. Genauso dürfen in lokalen Blöcken nicht auf Konstanten geschrieben werden. Es darf nur auf initialisierten Speicher zugegriffen werden und doppelte Initialisierungen sind nicht zulässig.

 Diese einheitliche Umsetzung soll durch die Struktur der Namensräume oder \emph{Environments} ermöglicht werden (Abbildung \ref{fig:environment}). In folgendem Abschnitt wird beschrieben wie die allgemeingültigen Kontext-Einschränkungen implementiert und wie diese für die Erweiterung angewandt werden sollen.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.75\linewidth]{assets/Environment.png}
		\caption{Environment-Klassendiagramm}
		\label{fig:environment}
	\end{center}
\end{figure}

Im Parser soll auf Ebene Programm ein erstes Environment-Objekt erstellt werden, dass den globalen Namensraum abbildet. Beim Antreffen von Deklarationen werden diese mit ihren Metainformationen in die jeweiligen Maps eingefügt. Die Maps fungieren als \emph{Symbol Tables}.  Beim Einlesen einer Routinendeklaration oder eines Let-Blocks wird ein neues Environment erstellt und mit dem \emph{Parent}, also dem äusseren Environment verknüpft.  Angewandte Identifier werden im Parser mit ihren lokalen Environment verknüpft. So kann in der statischen Analyse immer auf den lokalen und die übergeordneten \emph{Scopes} zugegriffen werden.

Beim Einfügen in eine Symbol-Table soll geprüft werden, ob schon ein gleichnamiger Eintrag in dieser vorhanden ist. Wenn dies der Fall ist, soll ein \emph{DuplicateDeclarationError} geworfen werden (vgl. Code-Beispiel \ref{lst:DuplicateDeclaration}). Das \emph{Parent}-Environment soll nicht beachtet werden. Beim Versuch einen angewandten Identifier auszulesen soll im lokalen Environment angefangen und, wenn nichts gefunden wurde, rekursiv im Parent nachgefragt werden. Falls in keinem Environment ein Eintrag gefunden wird, soll vom Checker ein \emph{UndeclaredIdentifierError} geworfen werden  (vgl. Code-Beispiel \ref{lst:UndeclaredIdentifier}).

Mit diesem Ansatz folgt die Unterstützung für \emph{Variable Shadowing}, also der Wiederverwendung von Namen aus einem äusseren Scope und dem Maskieren der äusseren Variable, direkt aus der Struktur des Environment. Es müssen keine zusätzlichen Überprüfungen gemacht werden, da lokale Environments zuerst untersucht werden. Vgl. Abschnitt \ref{sec:shadowing} für eine Diskussion von alternativen Ansätzen. 

Es soll allerdings auch erlaubt sein, dass Variablen in einem lokalen Block initialisiert und verändert werden können. Die Prüfungen, ob auf uninitialisierten Speicher geschrieben oder gelesen wird, muss also Initialisierungen in Let-Blöcken beachten. Für ein Beispiel hiervon, siehe Code-Listing \ref{lst:LetInit}.

\section{Code-Erzeugung}
Die Virtual Machine soll um den \emph{Let-Pointer (lp)} erweitert werden. Dieser dient dazu den Storage der Let-Deklarationen zu verwalten und adressieren. Die Idee dabei ist, dass die Let-Deklarationen nach ausgeführtem Let-Block wieder weggeräumt werden können. So braucht es keinen neuen Frame und es kann auf dessen Storage zugegriffen werden. Dadurch entfällt das Kopieren von lokalen Variablen auf einen eigenen Frame.

Unterhalb des Let-Pointer ist der Storagebereich der Let-Commands. Der Bereich oberhalb des Let-Pointer wird von den Commands für die Berechnungen verwendet.

Zu diesem Zweck werden folgende Instruktionen in der Virtual Machine eingeführt:

\lstinputlisting[caption={LetStore Instruktion}, firstline=191, lastline=199]{../src/main/kotlin/ch/fhnw/vm/VirtualMachine.kt}

Zuerst wird der Wert des Let-Pointers in den Store geschrieben, damit bei verschachtelten Let-Blocks später wieder darauf zugegriffen werden kann. Danach wird der Stack-Pointer dem Let-Pointer zugewiesen und der Stack-Pointer sowie der Programm-Counter erhöht.

\newpage

\lstinputlisting[caption={LoadAddrLet Instruktion}, firstline=200, lastline=210]{../src/main/kotlin/ch/fhnw/vm/VirtualMachine.kt}
In der LoadAddrLet Instruktion werden zuerst die Vorbedingungen geprüft. Die relativeAddress muss dabei kleiner 0 sein, da sie vom Let-Pointer aus gesehen weiter unten auf dem Stack liegt.
Danach kann der Wert in den Store geladen werden. Zum Schluss werden wieder Stack-Pointer und Programm-Counter erhöht.

\lstinputlisting[caption={EndLet Instruktion}, firstline=129, lastline=140]{../src/main/kotlin/ch/fhnw/vm/VirtualMachine.kt}
Am Ende des EndLet-Blocks muss der Stack-Pointer eins höher liegen als der Let-Pointer. Falls dem nicht so ist, wird ein ExecutionError geworfen.
Danach wird der Let-Pointer auf den Wert vor der Ausführung des aktuellen Let-Blocks gesetzt und der Stack-Pointer TopOfStack vor den Let-Block. Zum Schluss wird der Programm-Counter erhöht.

Die Erzeugung dieser Instruktionen ist in der folgenden Code-Auflistung gegeben: 
\lstinputlisting[caption={code-Methode der AbsLetCmd-Klasse}, firstline=95, lastline=107]{../src/main/kotlin/ch/fhnw/parser/abstract/AbsCmd.kt}
Zuerst werden die Variablen im LetBlock gezählt, damit eine entsprechende Anzahl von Speicherplätzen auf dem Stack reserviert werden kann. Danach folgt die Let-Store, welche den Let-Pointer an die richtige Stelle setzt. In der for-Schleife werden alle Instruktionen der Commands im Let-Block erzeugt. Zum Schluss wird das Ende des Let-Blocks mit der EndLet-Instruktion markiert.


Die Funktionalität dieser Erweiterungen soll anhand eines Beispiels verdeutlicht werden. Mit Code-Listing \ref{lst:Extension} (siehe Anhang) werden folgende Instruktionen erzeugt.

\begin{lstlisting}[caption={Generierte Instruktionen}, label={lst:instructions}]
	0: AllocBlock(size=0)
	1: Call(routineAddress=3, symbolicAddress='example')
	2: Stop()
	3: AllocBlock(size=2)
	4: LoadAddrRel(relAddress=3)
	5: LoadImInt(value=5)
	6: Store()
	7: LoadAddrRel(relAddress=4)
	8: LoadImInt(value=10)
	9: Store()
	10: AllocBlock(size=1)
	11: LetStore(size=1)
	12: LoadAddrLet(size=-1)
	13: LoadAddrRel(relAddress=4)
	14: Deref()
	15: LoadAddrRel(relAddress=3)
	16: Deref()
	17: MultInt()
	18: Store()
	19: LoadAddrLet(size=-1)
	20: Deref()
	21: OutputInt(indicator='Int')
	22: EndLet(numberOfDecls=1)
	23: Return(size=0)
\end{lstlisting}

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{assets/LetStore.png}
		\caption{VM-Zustand nach Ausführung der AllocBlock- und LetStore-Instruktionen}
		\label{fig:let-store}
	\end{center}
\end{figure}

Zur Laufzeit wird vor Erreichen der Let-Store-Instruktion ein Speicherplatz für die Let-Konstante \emph{e} angelegt. Nach Ausführung der LetStore-Instruktion wird der Let-Pointer auf den Speicherplatz nach der Let-Konstante gesetzt Der vorherige Wert Null wird an diese Stelle geschrieben. Siehe Abbildung \ref{fig:let-store} zur Visualisierung des Virtual Machine-Zustands.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{assets/LetCmd.png}
		\caption{VM-Zustand nach Ausführung der OutputInt-Instruktion}
		\label{fig:let-cmd}
	\end{center}
\end{figure}

Die Instruktionen 12 bis 21 bilden den Body des Let-Blocks ab. Nach Ausführung der Instruktionen liegt der Stack-Pointer wieder eine Stelle nach dem Let-Pointer. Die Konstante \emph{e} wurde gesetzt und verwendet.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{assets/EndLet.png}
		\caption{VM-Zustand nach Ausführung der EndLet-Instruktion}
		\label{fig:end-let}
	\end{center}
\end{figure}

Mit der Endlet-Instruktion (22) wird der Virtual-Machine-Zustand wieder zurückgesetzt. Dem Let-Pointer wird sein vorheriger Wert zugewiesen und der Speicher der Let-Konstante wird freigegeben.

\section{Vergleich mit anderen Programmiersprachen}
Bl"ocke mit lokalen Deklarationen sind in vielen gel"aufigen Programmiersprachen vorhanden. In C-basierten Sprachen werden Bl"ocke mithilfe von geschweiften Klammern erstellt. Beispielsweise die Java Language Specification definiert Bl"ocke folgendermassen:
\begin{quote}
	A block is a sequence of statements, local class declarations, and local variable declaration statements within braces. 
\end{quote}

Ein Block in Java definiert einen eigenen Scope. Auf die Variablen kann von aussen nicht zugegriffen werden und sie k"onnen Variablen eines "ausseren Scopes "uberschreiben. Ein Beispiel ist in der Auflistung~\ref{lst:java-block} aufgef"uhrt. 
\begin{lstlisting}[caption={Block mit einer lokalen Variable}, label={lst:java-block}]
{ //block start
	int x = 20;
	x++;
} //block end
\end{lstlisting}
Das Sprach-Feature ist also einigermassen beschr"ankt und wird ausserhalb von statischen Intialisierungs-Bl"ocken nicht gross verwendet. Hingegen sind im Lisp-Dialekt Clojure lokale Bl"ocke allgegenw"artig. Die \emph{let}-Funktion nimmt eine Liste von 
\emph{Bindings} und einen \emph{Body}, in dem die Bindings verwendet werden. Wie im Beispiel~\ref{lst:clj-shadow} gezeigt wird, unterst"utzt die \emph{let}-Funktion Shadowing.
\begin{lstlisting}[caption={Let mit Shadowing}, label={lst:clj-shadow}]
(let [a "aaa"]
  (println a)) ; prints aaa
  
(let [a "aaa"]
  (let [a "AAA"]
    (println a))) ; prints AAA
\end{lstlisting}
Man kann sogar die gleiche Variable mehrfach shadowen. Im Beispiel~\ref{lst:clj-multiple} wird eine typische Verwendung von let-Bl"ocken gezeigt, in der Daten im Binding-Schritt vorbereitet und im Body nur noch einfach verarbeitet werden. Durch das Sprach-Feature des \emph{Destructuring} und weiterer funktionaler Konzepte wird dieser Programmierstil beg"unstigt.
\begin{lstlisting}[caption={Mehrfaches Shadowing der gleichen Variable}, label={lst:clj-multiple}]
(let [s "Beispiel Code  "
  s (str/upper-case s)
  s (str/trim s)
  s (str/replace s #" +" "-")]
  (println s)) ; prints BEISPIEL-CODE
\end{lstlisting}

In der IML-Erweiterung soll Shadowing unterstützt werden, wie in \ref{sec:restrictions} beschrieben. Es ist allerdings nicht möglich eine Variable innerhalb eines Let-Blocks mehrfach zu deklarieren. Es wird wie in Clojure eine Trennung zwischen Deklarations- und Ausführungs-Block gemacht.

\section{Entwurfsalternativen}
\subsection{Lexikalische Syntax}
Anstatt der Erweiterung der Reserved Identifiers um \emph{let} und \emph{endlet}, h"atte auch \emph{local} wiederverwendet werden k"onnen. In der Grammatik folgt jedoch nach \emph{local} immer ein \emph{cpsStoDecl}. In dieser Erweiterung muss aber zwingend noch ein \emph{cpsCmd} folgen, da die lokalen Variablen auch benutzt werden sollen. Daher wurde entschieden, mit \emph{let} und \emph{endlet} ein neues Konstrukt einzubringen um \emph{local} nicht auf zwei verschiedene Arten zu verwenden.

\subsection{Shadowing und lokale Namensräume}
\label{sec:shadowing}
Wie in Abschnitt \ref{sec:restrictions} besprochen soll der Compiler Shadowing unterstützen. Für die Erweiterung hat dies die Auswirkung, dass Variablen unter Umständen mehrfach initialisiert werden können. Es ist nicht erlaubt Variablen, die ausserhalb eines Blocks deklariert sind, zuerst in lokalen Blöcken und anschliessend im beinhaltenden Block zu initialisieren, da nicht auf bereits initialisierten Speicher zugegriffen werden darf. Dieses Verhalten ist konsistent mit Standard-IML. Es darf allerdings im lokalen Block eine neue Variable mit gleichem Namen angelegt werden. Eine Initialisierung dieser hat keinen Einfluss auf die äussere Variable.

Es wäre auch möglich Änderungen an einer äusseren Variable nach Verlassen des lokalen Blocks rückgängig zu machen, also immer eine lokale Kopie anzulegen. So wäre es aber nicht mehr möglich mittels eines lokalen Blocks den Wert zweier Variablen zu tauschen. Ein Beispiel dafür ist im Code-Listing \ref{lst:Swap} zu sehen.

Shadowing nicht zu unterstützen, wäre nicht konsistent mit anderen Sprach-Features. So können zum Beispiel Programm-Parameter-Namen als Routinen-Parameter-Namen wiederverwendet werden.

% subsection symbol tables?

\subsection{Code-Erzeugung}
Alternativ zur Variante mit dem Let-Pointer wäre auch ein Konstrukt analog zu den Funktionen oder Prozeduren denkbar gewesen. Dann gäbe es für jeden Let-Block einen eigenen Frame. Dies hätte es notwendig gemacht, dass die lokalen Variablen jeweils in den Frame des Let-Blocks kopiert werden, da diese dort ebenfalls zur Verfügung stehen müssen.

Würde der Heap verwendet werden, hätte das Let-Konstrukt auch mit dessen Hilfe implementiert werden können. Da der Heap nicht verwendet wird, wurde dieser alternative Ansatz nicht weiter verfolgt.

Eine weitere Alternative wäre das Hinzufügen der Let-Deklarationen zu den lokalen Deklarationen gewesen. Die Let-Deklarationen hätten aber getrennt von den local Deklarationen verwaltet werden müssen. Dies hätte zu mehr Komplexität geführt. Die Zusammenführung wäre auch etwas widersprüchlich, da mit den Let-Blocks striktere Einschränkungen des Sichtbarkeitsbereichs von Deklarationen erreicht werden sollen.

\subsection{Unterschiede zu Compiler im Unterricht}
\subsubsection{BaseToken}
Anstatt einer Basisklasse \emph{BaseToken} wird ein Interface \emph{Token} verwendet. Dies führt dazu, dass jedes Token eine eigene Klasse ist, welche das Interface \emph{Token} implementiert. Dies wird so gehandhabt, damit die ReservedIdentifiers direkt am Typ der Klasse unterschieden werden können.\newline
Für die Token-Klassen werden Records  bzw. Kotlin Data-Klassen verwendet. Diese verlangen mindestens einen Parameter(Terminal) im Konstruktor. Bei einigen Token ist dies überflüssig, da der Parameter nur einen einzigen Wert annehmen kann. Für alle ReservedIdentifiers, für die nur ein einziges Terminal möglich ist, hätte auch eine generelle Klasse mit einem Terminal als Parameter gereicht.

\subsection{Unterschiede zu Standard-IML}
Unterschiede zum im Unterricht behandelten Standard-IML sind in der folgenden Tabelle (\ref{tab:differences}) aufgelistet.

\begin{table}[!ht]
	\caption{Unterschiede zu Standard-IML}
	\label{tab:differences}
	\begin{tabular}{|l|p{13cm}|}
		\hline
		\textbf{Unterschied} & \textbf{Beschreibung} \\
		\hline
		Integer & INT1024 wird als einzige INT-Variante unterstützt.  \\
		\hline
		Kommentare & Kommentare im Quellcode werden aktuell nicht unterst"utzt. \\
		\hline
		Division & Es werden nur truncated Division und truncated Modulo unterst"utzt. \\
		\hline
	\end{tabular}
\end{table}

\section{Ehrlichkeitserklärung}

Wir  erklären, dass  wir  die oben  genannte Arbeit  gemeinsam verfasst  haben  und  gemeinsam Verantwortung für den Text tragen. Wir haben nur erlaubte Hilfsmittel eingesetzt und alle Zitate als solche kenntlich gemacht. Den Code für den Compiler haben wir selbst verfasst. Als Inspiration dafür dienen der Code aus dem Unterricht und das Buch \emph{Compilers - Principles, Techniques, and Tools.}

\begin{tabular}{l p{2em} l p{2em} l}
	\hspace{3cm} && \hspace{3cm}       && \hspace{3cm} \\
	&& && \\
	Brugg, den 02.01.2021&&      
\includegraphics[width=10em]{assets/signature_pascal.png}
&&   \includegraphics[width=10em]{assets/signature_juerg.png}\\
&& && \\
	\cline{1-1} \cline{3-3} \cline{5-5}
	Ort, Datum   && Pascal Ellenberger && Jürg Gnos
\end{tabular}

\clearpage
\section{Anhang}
\subsection{IML-Testprogramme}
%zum Test aller relevanten Eigenschaften der Erweiterung, insbesondere der Kontext-und Typeinschränkungen
%keine Einschränkung der Länge dieses Anhangs


\lstinputlisting[label={lst:DuplicateDeclaration}, caption={Ungültiges Programm welches zu DuplicateDeclarationError führen soll}]{../src/main/resources/DuplicateDeclaration.iml}
\lstinputlisting[label={lst:UndeclaredIdentifier}, caption={Ungültiges Programm welches zu UndeclaredIdentifierError führen soll}]{../src/main/resources/UndeclaredIdentifier.iml}
\lstinputlisting[label={lst:LetInit}, caption={Initialisierung in lokalem Block}]{../src/main/resources/LetInit.iml}
\lstinputlisting[label={lst:Swap}, caption={Euclid mit Swap}]{../src/main/resources/Euclid.iml}
\lstinputlisting[label={lst:Extension}, caption={Zugriff auf äusseren Scope}]{../src/main/resources/Extension.iml}

\end{document}

